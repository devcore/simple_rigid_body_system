/*--------------------------------------------------------\
| File: Skybox.h                                          |
|x-------------------------------------------------------x|
| Details: Creates an inverse cube and loads a cubemap    |
| texture. Uses them to create a skybox by follwing the   |
| camera and disabling depth testing                      |
|x-------------------------------------------------------x|
| Author:                              Andrius Jefremovas |
| Description:  Creates a skybox & controls its rendering.|
\--------------------------------------------------------*/
// Note: move this to the render_framework
#ifndef SKY_BOX_H
#define SKY_BOX_H

#include <graphics_framework.h>

struct Skybox
{
    Skybox();
    ~Skybox();
    // Match the position of the camera, disable depth testing and render!
    void Render(const float totalTime, const graphics_framework::camera *cam);
private:
    graphics_framework::mesh m_mesh;
    graphics_framework::effect m_eff;
    graphics_framework::cubemap m_cubemap;
};

#endif/* !SKY_BOX_H */