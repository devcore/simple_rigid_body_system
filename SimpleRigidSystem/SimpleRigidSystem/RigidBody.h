/*--------------------------------------------------------\
| File: RigidBody.h                                       |
|x-------------------------------------------------------x|
| Details: Contains an abstract base rigidbody structure  |
| and derived rigid types: sphere, AABB, OBB and capsule. |
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:     Rigidbody difinition and derived types.|
\--------------------------------------------------------*/

#ifndef RIGID_BODY_H
#define RIGID_BODY_H

#include "Util.h"
#include "ColliderTypes.h"
#include "CollisionDetection.h"
#include "RigidPlane.h"

namespace rigid_body_sys
{
#define FLOAT_INF 1.1e+12F
#define FLOAT_EPSILON epsilon<float>()

// Abstract base class for rigid bodies
struct RigidBody
{
    // Linear components
    glm::vec3 position;    
    glm::vec3 linVelocity; 
    glm::vec3 acceleration;     
    // Angular components
    glm::quat orientation;
    glm::vec3 angVelocity;
    float invmass;
    glm::mat4 localInvInertia;
    glm::vec3 torque;
    // Combined components
    glm::mat4 matWorld;
    glm::mat4 worldInvInertia;
    // State and characteristic constants
    bool movable;
    const float RESTITUTION;   // [0, 1] 
    const float FRICTION;      // [0, 1] 
    /* Parameters: position, orientation, linear and angular velocity,
                    inverse mass, coeficients of restitution and friction,
                    graphcis resources.*/
    RigidBody(const glm::vec3 &pos,    const glm::vec3 &orient, 
              const glm::vec3 &linVel, const glm::vec3 &angVel,
              const float invmass,     const float restitution,
              const float friction,    GraphicsResouces *gr);
    // Pure virtual destructor to not allow creation without a collider
    virtual ~RigidBody() = 0;
    // Physics update call
    virtual void Update(const float timeStep) = 0;
    // Renderer render call
    virtual void Render(const float time, 
                        const graphics_framework::camera *cam) = 0;
    // Check if two rigid bodies are colliding
    virtual bool TestCollision(RigidBody &body, CollisionData &cd) = 0;
    virtual bool TestCollision(RigidBody &body, OBB &obb, CollisionData &cd) = 0;
    virtual bool TestCollision(RigidBody &body, AABB &aabb, CollisionData &cd) = 0;
    virtual bool TestCollision(RigidBody &body, Sphere &sphere, CollisionData &cd) = 0;
    // Separate case for plane
    virtual bool TestCollision(RigidPlane &plane, CollisionData &cd) = 0;
protected:
    // Integration constants
    const float m_GRAVITY;
    const float m_DAMPING;
    // Rendering properties: mesh, shader, lighting etc..
    GraphicsResouces* m_pGraphicRes;
}; 

}; /* !rigid_body_sys */

#endif /* !RIGID_BODY_H */