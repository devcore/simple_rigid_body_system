/*
File: Simulation.cpp
--------------------------------------------------------------------------------
For details see Simulation.h
*/
#include "Simulation.h"
#include "CollisionDetection.h"
#include "CollisionResponse.h"
#include "Layouts.h"

using namespace glm;
using namespace graphics_framework;
using namespace rigid_body_sys;

Simulation::Simulation()
{
    m_simulate = false;
    // Build graphics resources
    LoadGraphicResources();
    // Setup a test case
    LayoutDefault(m_rigidBodies, m_planeBuffer, m_graphics);
}

Simulation::~Simulation()
{
    for (uint i = 0; i < m_planeBuffer.size(); i++)
    {
        delete m_planeBuffer[i];
    }
    for (uint i = 0; i < m_rigidBodies.size(); i++)
    {
        delete m_rigidBodies[i];
    }
}

void Simulation::Input(const float deltaTime, InputHandler *ih)
{
    if (glfwGetKeyOnce(GLFW_KEY_1))
    {
        // Mixed body collision test
        LayoutMixWallTest(m_rigidBodies, m_planeBuffer, m_graphics);
        ih->SetCurrentCam(CameraSet::e_FREE_CAM);
        free_camera* cam{ static_cast<free_camera*>(ih->GetCurrentCam()) };
        cam->set_position(vec3(-100.0f, 50.0f, 0.0f));
        cam->set_pitch(-quarter_pi<float>()*0.3f);
        cam->set_yaw(-pi<float>()*0.5f);
        m_simulate = false;

    }
    if (glfwGetKeyOnce(GLFW_KEY_2))
    {
        // Sphere bounce test
        LayoutSphereSmashTest(m_rigidBodies, m_planeBuffer, m_graphics);
        ih->SetCurrentCam(CameraSet::e_FREE_CAM);
        free_camera* cam{ static_cast<free_camera*>(ih->GetCurrentCam()) };
        cam->set_position(vec3(0.0f, 55.0f, 100.0f));
        cam->set_pitch(-quarter_pi<float>()*0.25f);
        cam->set_yaw(0.0f);
        m_simulate = false;
    }
    if (glfwGetKeyOnce(GLFW_KEY_3))
    {
        // Slope friction test
        LayoutFrictionTest(m_rigidBodies, m_planeBuffer, m_graphics);
        ih->SetCurrentCam(CameraSet::e_FREE_CAM);
        free_camera* cam{ static_cast<free_camera*>(ih->GetCurrentCam()) };
        cam->set_position(vec3(-10.0f, 0.0f, 110.0f));
        cam->set_pitch(0.0f);
        cam->set_yaw(0.0f);
        m_simulate = false;
    }
    if (glfwGetKeyOnce(GLFW_KEY_4))
    {
        // Local Inertia matrix test
        LayoutSpinOBBTest(m_rigidBodies, m_planeBuffer, m_graphics);
        ih->SetCurrentCam(CameraSet::e_FREE_CAM);
        free_camera* cam{ static_cast<free_camera*>(ih->GetCurrentCam()) };
        cam->set_position(vec3(0.0f, 20.0f, 60.0f));
        cam->set_pitch(-quarter_pi<float>()*0.25f);
        cam->set_yaw(0);
        m_simulate = false;
    }
    if (glfwGetKeyOnce(GLFW_KEY_SPACE))
    {
        m_simulate = !m_simulate;
    }
}

void Simulation::Update(const float timeStep)
{
    // Clear previous contact points
    dbgPoints.clear();
    if (m_simulate)
    {
        // Integrate rigid bodies
        for (uint i = 0; i < m_rigidBodies.size(); i++)
        {
            m_rigidBodies[i]->Update(timeStep);
        }
        // Test against planes
        for (uint i = 0; i < m_rigidBodies.size(); i++)
        for (uint j = 0; j < m_planeBuffer.size(); j++)
        {
            CollisionData cd;
            if (m_rigidBodies[i]->TestCollision(*m_planeBuffer[j], cd))
            {
                dbgPoints.push_back(cd.point); // Store contact point
            }
        }
        // Test against each other
        for (uint i = 0; i < m_rigidBodies.size(); i++)
        for (uint j = 0; j < m_rigidBodies.size(); j++)
        {
            CollisionData cd;
            if (i!=j && m_rigidBodies[i]->TestCollision(*m_rigidBodies[j], cd))
            {
                dbgPoints.push_back(cd.point); // Store contact point
            }
        }
    }
}

void Simulation::Render(const float time, const camera *cam)
{
    // Render rigid bodies
    for (uint i = 0; i < m_rigidBodies.size(); ++i)
    {
        m_rigidBodies[i]->Render(time, cam);
    }
    // Render collision boxes at debug points
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    for (uint i = 0; i < dbgPoints.size(); ++i)
    {
        // Calculate MVP
        m_graphics[e_DBG_PT].mesh.get_transform().position = dbgPoints[i];
        const mat4 M = m_graphics[e_DBG_PT].mesh.get_transform().get_transform_matrix();
        const mat4 MVP = P * V * M;
        // Bind basic texture shader and send uniforms
        renderer::bind(m_graphics[e_DBG_PT].eff);
        glUniformMatrix4fv(m_graphics[e_DBG_PT].eff.get_uniform_location("MVP"),
            1, GL_FALSE, glm::value_ptr(MVP));
        glUniform4fv(m_graphics[e_DBG_PT].eff.get_uniform_location("DBG_COL"),
            1, value_ptr(vec4(5.0f, 0.1f, 0.1f, 1.0f)));
        renderer::bind(m_graphics[e_DBG_PT].tex, 0); // Bind blueprint texture
        renderer::render(m_graphics[e_DBG_PT].mesh); // Render dbg box
    }
    // Render planes after to not deal with transperancy
    RigidPlane::Render(cam, m_planeBuffer);

}

void Simulation::LoadGraphicResources()
{
    // Debug texture shader
    effect dbgTexture; 
    dbgTexture.add_shader("..\\Resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
    dbgTexture.add_shader("..\\Resources\\shaders\\dbg.frag", GL_FRAGMENT_SHADER);
    dbgTexture.build();
    // Debug texture shader
    effect phong;
    phong.add_shader("..\\Resources\\shaders\\phong.vert", GL_VERTEX_SHADER);
    phong.add_shader("..\\Resources\\shaders\\phong.frag", GL_FRAGMENT_SHADER);
    phong.build();
    // material for rigid bodies
    material mat; 
    mat.set_emissive(vec4(0.0f, 0.07f, 0.07f, 1.0f));
    mat.set_diffuse( vec4(0.0f, 0.7f,   0.7f, 1.0f));
    mat.set_specular(vec4(0.0f, 0.7f,   0.7f, 1.0f));
    mat.set_shininess(25.0f);
    // Scene light attributes
    directional_light light; 
    light.set_ambient_intensity(vec4(0.2f, 0.2f, 0.2f, 1.0f));
    light.set_light_colour(     vec4(1.0f, 1.0f, 1.0f, 1.0f));
    light.set_direction(normalize(vec3(-1.0f, 1.0f, 1.0f)));
    // Rigid sphere
    m_graphics[e_SPHERE].mesh.set_geometry(geometry_builder::create_sphere());
    m_graphics[e_SPHERE].eff = phong;
    m_graphics[e_SPHERE].tex = texture("..\\resources\\textures\\blueprint_sphere.png");
    m_graphics[e_SPHERE].mat = mat;
    m_graphics[e_SPHERE].light = light;
    // Rigid plane
    m_graphics[e_PLANE].mesh.set_geometry(geometry_builder::create_plane(10, 10));
    m_graphics[e_PLANE].eff = dbgTexture;
    m_graphics[e_PLANE].tex = texture("..\\resources\\textures\\blueprint_plane.png");
    m_graphics[e_PLANE].mat = mat;
    m_graphics[e_PLANE].light = light;
    // Rigid AABB 
    m_graphics[e_AABB].mesh.set_geometry(geometry_builder::create_box());
    m_graphics[e_AABB].eff = phong;
    m_graphics[e_AABB].tex = texture("..\\resources\\textures\\blueprint_aabb.png");
    mat.set_emissive(vec4(0.0f, 0.01f, 0.0f, 1.0f));
    mat.set_diffuse(vec4( 0.0f, 0.7f,  0.0f, 1.0f));
    mat.set_specular(vec4(0.0f, 0.7f,  0.0f, 1.0f));
    m_graphics[e_AABB].mat = mat;
    m_graphics[e_AABB].light = light;
    // Rigid OBB
    m_graphics[e_OBB].mesh.set_geometry(geometry_builder::create_box());
    m_graphics[e_OBB].eff = phong;
    m_graphics[e_OBB].tex = texture("..\\resources\\textures\\blueprint_obb.png");
    mat.set_emissive(vec4(0.1f, 0.05f, 0.0f, 1.0f));
    mat.set_diffuse(vec4( 1.0f, 0.5f,  0.0f, 1.0f));
    mat.set_specular(vec4(1.0f, 0.5f,  0.0f, 1.0f));
    m_graphics[e_OBB].mat = mat;
    m_graphics[e_OBB].light = light;
    // Debug points
    m_graphics[e_DBG_PT].mesh.set_geometry(geometry_builder::create_sphere());
    m_graphics[e_DBG_PT].eff = dbgTexture;
    m_graphics[e_DBG_PT].tex = texture("..\\resources\\textures\\blueprint.png");
    m_graphics[e_DBG_PT].mat = mat;
    m_graphics[e_DBG_PT].light = light;
}