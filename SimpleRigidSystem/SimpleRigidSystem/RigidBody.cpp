/*
File: RigidBody.cpp
--------------------------------------------------------------------------------
For details see RigidBody.h
*/
#include "RigidBody.h"

using namespace glm;
using namespace rigid_body_sys;

RigidBody::RigidBody(const vec3 &pos,     const vec3 &orient, 
                    const vec3 &linVel,   const vec3 &angVel,
                    const float invmass,  const float restitution,
                    const float friction, GraphicsResouces *gr) :
                    position(pos),            orientation(quat(orient)),
                    linVelocity(linVel),      angVelocity(angVel),
                    acceleration(vec3(0.0f)), torque(vec3(0.0f)),
                    invmass(invmass),         movable(true),                        
                    RESTITUTION(restitution), FRICTION(friction),
                    m_GRAVITY(-9.8f),         m_DAMPING(0.99987f),
                    m_pGraphicRes(gr)
{
    assert("Coeficient of restitution must be [0, 1]" &&
        restitution >= 0.0f && 1.0f >= restitution);
    // Disabled for a test case
    //assert("Friction coeficient must be [0, 1]" &&
    //    friction >= 0.0f && 1.0f >= friction);
}

RigidBody::~RigidBody() { /* Abstract: do not allow creation with no type */ }