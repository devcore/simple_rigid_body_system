/*
File: Skybox.cpp
--------------------------------------------------------------------------------
For details see Skybox.h
*/
#include "Skybox.h"

using namespace glm;
using namespace graphics_framework;

Skybox::Skybox()
{
    // Generate inverse cube
    geometry geom;
    geom.set_type(GL_QUADS);
    std::vector<vec3> vertices
    { // Cube
        vec3(-1.0f, -1.0f, -1.0f),
        vec3(1.0f, -1.0f, -1.0f),
        vec3(1.0f, 1.0f, -1.0f),
        vec3(-1.0f, 1.0f, -1.0f),
        
        vec3(1.0f, -1.0f, 1.0f),
        vec3(-1.0f, -1.0f, 1.0f),
        vec3(-1.0f, 1.0f, 1.0f),
        vec3(1.0f, 1.0f, 1.0f),
        
        vec3(1.0f, -1.0f, -1.0f),
        vec3(1.0f, -1.0f, 1.0f),
        vec3(1.0f, 1.0f, 1.0f),
        vec3(1.0f, 1.0f, -1.0f),
        
        vec3(-1.0f, -1.0f, 1.0f),
        vec3(-1.0f, -1.0f, -1.0f),
        vec3(-1.0f, 1.0f, -1.0f),
        vec3(-1.0f, 1.0f, 1.0f),
        
        vec3(-1.0f, -1.0f, 1.0f),
        vec3(1.0f, -1.0f, 1.0f),
        vec3(1.0f, -1.0f, -1.0f),
        vec3(-1.0f, -1.0f, -1.0f),
        
        vec3(-1.0f, 1.0f, -1.0f),
        vec3(1.0f, 1.0f, -1.0f),
        vec3(1.0f, 1.0f, 1.0f),
        vec3(-1.0f, 1.0f, 1.0f)
    };
    // Add VOB and add geom to mesh container
    geom.add_buffer(vertices, BUFFER_INDEXES::POSITION_BUFFER);
    m_mesh.set_geometry(geom);

    std::array<std::string, 6> filenames =
    {
        "..\\resources\\textures\\blueprint.png", //+x
        "..\\resources\\textures\\blueprint.png", //-x
        "..\\resources\\textures\\blueprint.png", //+y
        "..\\resources\\textures\\blueprint.png", //-y
        "..\\resources\\textures\\blueprint.png", //+z
        "..\\resources\\textures\\blueprint.png"  //-z
    };
    m_cubemap = cubemap(filenames);

    // Skybox shader program
    m_eff.add_shader("..\\Resources\\shaders\\skybox.vert", GL_VERTEX_SHADER);
    m_eff.add_shader("..\\Resources\\shaders\\skybox.frag", GL_FRAGMENT_SHADER);
    m_eff.build();
}

Skybox::~Skybox() 
{ 
    std::cerr << "LOG - Skybox Destroyed" << std::endl; 
}

void Skybox::Render(const float totalTime, const camera *cam)
{
    // Disable depth test & mask
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    // Bind shader
    renderer::bind(m_eff); 
    // Match with camera position
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    const mat4 M = scale(translate(mat4(1.0), cam->get_position()), vec3(10.0f));
    const mat4 MVP = P * V * M;  // Calculate MVP
    // Set uniforms
    glUniformMatrix4fv(
        m_eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
    glUniform1f(m_eff.get_uniform_location("time"), totalTime);
    renderer::bind(m_cubemap, 0); // Bind cubemap
    renderer::render(m_mesh); // Render the mesh!
    // Re-Enable depth test & mask
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
}