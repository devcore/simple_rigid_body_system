/*
File: RigidOBB.cpp
--------------------------------------------------------------------------------
For details see RigidOBB.h
*/
#include "RigidSphere.h"
#include "CollisionResponse.h"

using namespace glm;
using namespace graphics_framework;
using namespace rigid_body_sys;

RigidSphere::RigidSphere(const vec3 &pos,        const vec3 &orient, 
                         const vec3 &linVel,      const vec3 &angVel,
                         const float radius,      const float invmass,
                         const float restitution, const float friction,    
                         GraphicsResouces *gr) :
                         RigidBody(pos, orient, linVel, angVel, invmass,
                                   restitution, friction, gr),
                         m_sphere(pos, radius)
{
    // Compute inverse inertia matrix
    float invXYZ = radius*radius;
    invXYZ *= 2.0f;
    // Value taken from http://www.livephysics.com/physical-constants/mechanics-pc/moment-inertia-uniform-objects/
    invXYZ = 1.0f / ((invXYZ + invXYZ) / (invmass * 5.0f));
    localInvInertia = mat4{ invXYZ,   0.0f,   0.0f, 0.0f,
                              0.0f, invXYZ,   0.0f, 0.0f,
                              0.0f,   0.0f, invXYZ, 0.0f,
                              0.0f,   0.0f,   0.0f, 1.0f };
    UpdateMatrix();
}

void RigidSphere::Update(const float timeStep)
{
    if (movable)
    {
        UpdateMatrix();
        Integrate(timeStep);
        // Match the sphere volume
        m_sphere.position = position;
    }
}

void RigidSphere::Render(const float time, const camera *cam)
{
    // Set mesh to new location and calculate model view projection matrix
    m_pGraphicRes->mesh.get_transform().position = position;
    m_pGraphicRes->mesh.get_transform().orientation = orientation;
    m_pGraphicRes->mesh.get_transform().scale = vec3(m_sphere.radius);
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    const mat4 M = m_pGraphicRes->mesh.get_transform().get_transform_matrix();
    const mat4 MVP = P * V * M;
    // Bind shader and send uniforms
    renderer::bind(m_pGraphicRes->eff);
    glUniformMatrix4fv(m_pGraphicRes->eff.get_uniform_location("MVP"),
                       1, GL_FALSE, glm::value_ptr(MVP));
    glUniformMatrix4fv(m_pGraphicRes->eff.get_uniform_location("M"),
                       1, GL_FALSE, value_ptr(M));
    glUniformMatrix3fv(m_pGraphicRes->eff.get_uniform_location("N"),
                      1, GL_FALSE, value_ptr(
                      m_pGraphicRes->mesh.get_transform().get_normal_matrix()));
    glUniform1f(m_pGraphicRes->eff.get_uniform_location("time"), time);
    glUniform3fv(m_pGraphicRes->eff.get_uniform_location("eye_pos"), 
                 1, value_ptr(cam->get_position()));
    // Bind material, light and texture
    renderer::bind(m_pGraphicRes->mat, "mat");
    renderer::bind(m_pGraphicRes->light, "light");
    renderer::bind(m_pGraphicRes->tex, 0);
    // Render mesh
    renderer::render(m_pGraphicRes->mesh);
}

void RigidSphere::Integrate(const float timeStep)
{
    // Sanity checks
    assert(invmass > FLOAT_EPSILON);
    assert(length(acceleration) < FLOAT_INF);
    //linVelocity = clamp(linVelocity, -100.0f, 100.0f);
    // Apply gravity
    //acceleration.y -= GRAVITY;
    AddForce(position, vec3(0.0f, m_GRAVITY, 0.0f));
    // Semi-implicit Euler integration
    linVelocity += acceleration * invmass * timeStep;
    position += linVelocity * timeStep;
    angVelocity += vec3(worldInvInertia * vec4(torque, 1.0f));
    // Orientation
    quat spin = (quat(0.0f, angVelocity) * 0.5f) * orientation;
    orientation += spin * timeStep;
    orientation = normalize(orientation);
    // Reset acceleration, torque and apply damping
    acceleration = vec3(0.0f);
    torque = vec3(0.0f);
    linVelocity *= m_DAMPING;
    angVelocity *= m_DAMPING;
}

void RigidSphere::UpdateMatrix()
{
    mat4 mR = toMat4(orientation);
    mat4 mT = glm::translate(mT, position);
    matWorld = mR * mT;
    worldInvInertia = transpose(mR) * localInvInertia * mR;
}

void RigidSphere::AddForce(const vec3 &point, const vec3 &force)
{
    assert(invmass > FLOAT_EPSILON);
    acceleration += force * (1.0f / invmass);
    torque += cross(point - position, force);
}

bool RigidSphere::TestCollision(RigidPlane &planeBody, CollisionData &cd)
{
    if (SpherePlane(m_sphere, planeBody.plane, cd))
    {
        ApplyCollisionImpulse(*this, planeBody, cd);
        return true;
    }
    return false;
}

bool RigidSphere::TestCollision(RigidBody &body, CollisionData &cd)
{
    return (body.TestCollision(*this, m_sphere, cd));
}

bool RigidSphere::TestCollision(RigidBody &body, Sphere &sphere, CollisionData &cd)
{
    if (SphereSphere(sphere, m_sphere, cd))
    {
        ApplyCollisionImpulse(body, *this, cd);
        return true;
    }
    return false;
}

bool RigidSphere::TestCollision(RigidBody &body, AABB &aabb, CollisionData &cd)
{
    if (SphereAABB(m_sphere, aabb, cd))
    {
        ApplyCollisionImpulse(*this, *static_cast<RigidAABB*>(&body), cd);
        return true;
    }
    return false;
}

bool RigidSphere::TestCollision(RigidBody &body, OBB &obb, CollisionData &cd)
{
    if (SphereOBB(m_sphere, obb, cd))
    {
        ApplyCollisionImpulse(body, *this, cd);
        return true;
    }
    return false;
}