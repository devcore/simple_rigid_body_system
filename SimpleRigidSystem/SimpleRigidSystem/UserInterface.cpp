/*
File: UserInterface.cpp
--------------------------------------------------------------------------------
For details see UserInterface.h
*/
#include "UserInterface.h"

#ifdef _WIN32
#undef APIENTRY
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include <GLFW/glfw3native.h>
#endif

using namespace graphics_framework;
using namespace rigid_body_sys;

UserInterface::UserInterface() :
    m_FontTexture(0), m_VboHandle(0), m_VaoHandle(0), m_ElementsHandle(0),
    m_ShaderHandle(0), m_VertHandle(0), m_FragHandle(0), m_AttribLocTex(0),
    m_AttribLocProjMtx(0), m_AttribLocPos(0), m_AttribLocUV(0), m_AttribLocColor(0),
    m_orthoProj{ 2.0f / (float)renderer::get_screen_width(),   0.0f, 0.0f, 0.0f,
                 0.0f, 2.0f / -(float)renderer::get_screen_height(), 0.0f, 0.0f,
                 0.0f, 0.0f, -1.0f, 0.0f,
                -1.0f, 1.0f,  0.0f, 1.0f }
{
    ImGuiInitialise();
}

UserInterface::~UserInterface()
{
    // Destroy ImGUI
    ImGuiShutdown();
}

void UserInterface::ImGuiInitialise()
{
    // Setup display | Note: set every frame for window resizing
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize = ImVec2((float)renderer::get_screen_width(),
                            (float)renderer::get_screen_height());
    io.DisplayFramebufferScale = ImVec2(1.0f, 1.0f);
    // Setup draw lists
    //io.RenderDrawListsFn = ImGui_RenderDrawLists;
    io.RenderDrawListsFn = nullptr;
    io.ImeWindowHandle = glfwGetWin32Window(renderer::get_window());
    // Create shaders and load default font
    ImGuiCreateDeviceObjects();
}

void UserInterface::ImGuiShutdown()
{
    if (m_VaoHandle) glDeleteVertexArrays(1, &m_VaoHandle);
    if (m_VboHandle) glDeleteBuffers(1, &m_VboHandle);
    if (m_ElementsHandle) glDeleteBuffers(1, &m_ElementsHandle);
    m_VaoHandle = m_VboHandle = m_ElementsHandle = 0;

    glDetachShader(m_ShaderHandle, m_VertHandle);
    glDeleteShader(m_VertHandle);
    m_VertHandle = 0;

    glDetachShader(m_ShaderHandle, m_FragHandle);
    glDeleteShader(m_FragHandle);
    m_FragHandle = 0;

    glDeleteProgram(m_ShaderHandle);
    m_ShaderHandle = 0;

    if (m_FontTexture)
    {
        glDeleteTextures(1, &m_FontTexture);
        ImGui::GetIO().Fonts->TexID = 0;
        m_FontTexture = 0;
    }
    ImGui::Shutdown();
}

void UserInterface::ImGuiRenderDrawLists(camera &cam)
{
    // Setup render state
    // alphaBlending enabled, no face culling, no depth testing, scissor enabled
    TempInfoWidget();
    m_drawData = ImGui::GetDrawData();

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    glActiveTexture(GL_TEXTURE0);

    glUseProgram(m_ShaderHandle);
    glUniform1i(m_AttribLocTex, 0);
    glUniformMatrix4fv(m_AttribLocProjMtx, 1, GL_FALSE, glm::value_ptr(m_orthoProj));
    glBindVertexArray(m_VaoHandle);

    for (int n = 0; n < m_drawData->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = m_drawData->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;

        glBindBuffer(GL_ARRAY_BUFFER, m_VboHandle);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.size() * sizeof(ImDrawVert), (GLvoid*)&cmd_list->VtxBuffer.front(), GL_STREAM_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ElementsHandle);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx), (GLvoid*)&cmd_list->IdxBuffer.front(), GL_STREAM_DRAW);

        for (const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin(); pcmd != cmd_list->CmdBuffer.end(); pcmd++)
        {
            if (pcmd->UserCallback)
            {
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
                glScissor((int)pcmd->ClipRect.x, (int)(renderer::get_screen_height() - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
                glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, GL_UNSIGNED_SHORT, idx_buffer_offset);
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }

    // Restore modified GL state
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_SCISSOR_TEST);
}

void UserInterface::TempInfoWidget()
{
    ImGui::NewFrame();  // NewInputFrame

    // Information widget
    {
        ImGui::Begin("Information Widget");
        ImGui::Separator();
        ImGui::Text("Control information for the application: \n");
        ImGui::Separator();
        ImGui::Text("MOVEMENT KEYS: \n"\
                    "WASD  - Directional movement \n"\
                    "QE    - Up and Down movement \n"\
                    "SHIFT - Movement speed 15x increase (hold) \n"\
                    "Mouse - Rotation (Arrow keys work as well) \n"\
                    "SPACE - Pause simulation \n"\
                    "ALT   - Unlock mouse \n");
        ImGui::Separator();
        ImGui::Text("SCENE SHORCUTS: \n"\
                    "1 - Restitution test case \n"\
                    "2 - Mixed wall smash test case \n"\
                    "3 - Friction slide test case \n"\
                    "4 - Rotation, inertia tesor test case");
        ImGui::Separator();
    }

    //ImGui::Text("Select type:"); ImGui::SameLine();
    //ImGui::PushItemWidth(160);
    ////ImGui::Combo("", &item, items, IM_ARRAYSIZE(items));
    //ImGui::SameLine(0, 10); ImGui::PopItemWidth();
    //if (ImGui::Button("Add")) item = 2; 
    //ImGui::Separator(); // ---
    //ImGui::Text("Average %.3f ms/frame (%.1f FPS)",
    //    1000.0f / ImGui::GetIO().Framerate,
    //    ImGui::GetIO().Framerate);
    //ImGui::Separator(); // ---
    ImGui::End();
    ImGui::Render();
}

void UserInterface::ImGuiCreateDeviceObjects()
{
    // Backup GL state
    GLint last_texture, last_array_buffer, last_vertex_array;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);

    const GLchar *vertex_shader =
        "#version 330\n"
        "uniform mat4 ProjMtx;\n"
        "in vec2 Position;\n"
        "in vec2 UV;\n"
        "in vec4 Color;\n"
        "out vec2 Frag_UV;\n"
        "out vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	Frag_UV = UV;\n"
        "	Frag_Color = Color;\n"
        "	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
        "}\n";

    const GLchar* fragment_shader =
        "#version 330\n"
        "uniform sampler2D Texture;\n"
        "in vec2 Frag_UV;\n"
        "in vec4 Frag_Color;\n"
        "out vec4 Out_Color;\n"
        "void main()\n"
        "{\n"
        "	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
        "}\n";

    m_ShaderHandle = glCreateProgram();
    m_VertHandle = glCreateShader(GL_VERTEX_SHADER);
    m_FragHandle = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(m_VertHandle, 1, &vertex_shader, 0);
    glShaderSource(m_FragHandle, 1, &fragment_shader, 0);
    glCompileShader(m_VertHandle);
    glCompileShader(m_FragHandle);
    glAttachShader(m_ShaderHandle, m_VertHandle);
    glAttachShader(m_ShaderHandle, m_FragHandle);
    glLinkProgram(m_ShaderHandle);

    m_AttribLocTex = glGetUniformLocation(m_ShaderHandle, "Texture");
    m_AttribLocProjMtx = glGetUniformLocation(m_ShaderHandle, "ProjMtx");
    m_AttribLocPos = glGetAttribLocation(m_ShaderHandle, "Position");
    m_AttribLocUV = glGetAttribLocation(m_ShaderHandle, "UV");
    m_AttribLocColor = glGetAttribLocation(m_ShaderHandle, "Color");

    glGenBuffers(1, &m_VboHandle);
    glGenBuffers(1, &m_ElementsHandle);

    glGenVertexArrays(1, &m_VaoHandle);
    glBindVertexArray(m_VaoHandle);
    glBindBuffer(GL_ARRAY_BUFFER, m_VboHandle);
    glEnableVertexAttribArray(m_AttribLocPos);
    glEnableVertexAttribArray(m_AttribLocUV);
    glEnableVertexAttribArray(m_AttribLocColor);

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
    glVertexAttribPointer(m_AttribLocPos, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
    glVertexAttribPointer(m_AttribLocUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
    glVertexAttribPointer(m_AttribLocColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
#undef OFFSETOF
    ImGuiCreateFontsTexture();
    // Restore modified GL state
    glBindTexture(GL_TEXTURE_2D, last_texture);
    glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
    glBindVertexArray(last_vertex_array);
}

IMGUI_API void UserInterface::ImGuiCreateFontsTexture()
{
    ImGuiIO& io = ImGui::GetIO();
    // Build texture atlas
    unsigned char* pixels;
    int width, height;
    // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
    // Create OpenGL texture
    glGenTextures(1, &m_FontTexture);
    glBindTexture(GL_TEXTURE_2D, m_FontTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    // Store our identifier
    io.Fonts->TexID = (void *)(intptr_t)m_FontTexture;
    // Cleanup (don't clear the input data if you want to append new fonts later)
    io.Fonts->ClearInputData();
    io.Fonts->ClearTexData();
}