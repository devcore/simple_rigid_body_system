/*--------------------------------------------------------\
| File: SupportFunctions.h                                |
-----------------------------------------------------------
| Details: Contains support functions that find closest   |
| points, distances, extreme vertices. The implementation |
| most of these algorithms follow Christer Ericson's book:|
| Real-Time Collision Detection.                          |
|---------------------------------------------------------|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
\--------------------------------------------------------*/

// Algorithms follow Christer Ericson's book: Real - Time Collision Detection.

#ifndef SUPPORT_FUNCTIONS_H
#define SUPPORT_FUNCTIONS_H

#include "ColliderTypes.h"

namespace rigid_body_sys
{
    // Finds distance between a point and a plane
    inline float DistancePointPlane(const glm::vec3 &point, const Plane &plane)
    {
        assert(glm::length(plane.normal) == 1.0f && "Plane must be normalised");
        return (glm::dot(point - plane.position, plane.normal));
    }

    // Finds closest point on a plane
    inline glm::vec3 ClosestPointPlane(const glm::vec3 &p, const Plane &plane)
    {
        assert(glm::length(plane.normal) == 1.0f && "Plane must be normalised");
        float t = glm::dot(p - plane.position, plane.normal);
        return (p - t * plane.normal);
    }

    // Finds distance between a point and segment AB
    inline float SqDistancePointSegment(const glm::vec3 &point,
                                        const glm::vec3 &segA, 
                                        const glm::vec3 &segB)
    {
        glm::vec3 segAB = segB - segA;
        glm::vec3 distA = point - segA;
        glm::vec3 distB = point - segB;
        // Cases where projection is outside of segAB
        float out0 = glm::dot(distA, segAB);
        if (out0 <= 0.0f) return glm::dot(distA, distA);
        float out1 = glm::dot(segAB, segAB);
        if (out0 >= out1) return glm::dot(distB, distB);
        // Case where projection is on segAB
        return glm::dot(distA, distA) - out0*out0 / out1;
    }

    // Finds closest point on segment
    inline glm::vec3 ClosestPointSegment(const glm::vec3 &point,
                                         const glm::vec3 &segA,
                                         const glm::vec3 &segB,
                                         float &t)
    {
        glm::vec3 segAB = segB - segA;
        // Project the point onto segAB
        t = glm::dot(point - segA, segB) / glm::dot(segAB, segAB);
        // If outside of segment - clamp t to closest endpoint
        t = glm::clamp(t, 0.0f, 1.0f);
        // Compute the projected position of t
        return (segA + t*segAB);
    }

    // Find closest point on segements
    inline glm::vec3 ClosestPointSegmentSegment()
    {
        // Note: import from notes.h
        return glm::vec3(0.0f);
    }

    // Find square distance between a point and a axis aligned bounding box
    inline float SqDistancePointAABB(const glm::vec3 &point, const AABB &aabb)
    {
        float sqDist = 0.0; // Start with 0 and increment
        glm::vec3 aMin = aabb.position - aabb.hExtent;
        glm::vec3 aMax = aabb.position + aabb.hExtent;
        // For each axis count any excess distance outside of box
        for (glm::uint i = 0; i < 3; i++)
        {
            if (point[i] < aMin[i])
                sqDist += (aMin[i] - point[i]) * (aMin[i] - point[i]);
            if (point[i] > aMax[i])              
                sqDist += (aMax[i] - point[i]) * (aMax[i] - point[i]);
        }
        return sqDist;
    }

    // Finds a point closest on a axis aligned bounding box
    inline glm::vec3 ClosestPointAABB(const glm::vec3 &point, const AABB &aabb)
    {
        // For each axis clamp the point to the box
        glm::vec3 closestPoint;
#if 1 // Note: test effiency
        for (glm::uint i = 0; i < 3; i++)
        {
            closestPoint[i] = point[i];
            closestPoint[i] = glm::max(closestPoint[i],
                              aabb.position[i] - aabb.hExtent[i]);
            closestPoint[i] = glm::min(closestPoint[i],
                              aabb.position[i] + aabb.hExtent[i]);

        }
#else
        closestPoint = point;
        closestPoint = glm::min(glm::max(closestPoint, 
                                         aabb.position - aabb.hExtent),
                                         aabb.position + aabb.hExtent);
#endif
        return closestPoint;
    }

    // Find square distance between a point and a oriented bounding box
    inline float SqDistancePointOBB(const glm::vec3 &point, const OBB &obb)
    {
        glm::vec3 dist = point - obb.position;
        float sqDist = 0.0f;
        for (glm::uint i = 0; i < 3; i++)
        {
            // Project vector from box to point on each axis to get the distance
            // of the point along the axis adding to the excess
            float d = glm::dot(dist, obb.uAxis[i]), excess = 0.0f;
            if (d < -obb.hExtent[i]) excess = d + obb.hExtent[i];
            else if (d > obb.hExtent[i]) excess = d - obb.hExtent[i];
            sqDist += excess * excess;
        }
        return sqDist;
    }

    // Finds a point closest on a oriented bounding box
    inline glm::vec3 ClosestPointOBB(const glm::vec3 &point, const OBB &obb)
    {
        // Start result at center of the box and move out
        glm::vec3 closestPoint = obb.position;
        glm::vec3 d = point - closestPoint;
        // For each OBB axis
        for (glm::uint i = 0; i < 3; i++)
        {
            // Project d onto this axis to get distnace along it
            float dist = glm::dot(d, obb.uAxis[i]);
            // If distance farther than the box extents, clamp to the box
            // Step that distnace along the axis to get word coords
#if 1
            closestPoint += glm::clamp(glm::dot(d, obb.uAxis[i]), 
                                -obb.hExtent[i], obb.hExtent[i]) * obb.uAxis[i];
#else
            dist = glm::min(dist, obb.halfExtent[i]);
            dist = glm::max(dist, -obb.halfExtent[i]);
            if (dist > obb.hExtent[i]) dist = obb.hExtent[i];
            if (dist < -obb.hExtent[i]) dist = -obb.hExtent[i];
            closestPoint += dist * obb.uAxis[i];
#endif
        }
        return closestPoint;
    }

}; /* !rigid_body_sys */

#endif /* !SUPPORT_FUNCTIONS_H */
