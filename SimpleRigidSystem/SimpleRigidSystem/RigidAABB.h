/*--------------------------------------------------------\
| File: RigidAABB.h                                       | 
|x-------------------------------------------------------x| 
| Details: Rigid AABB is a derived class that uses a axis | 
| aligned bounding box collider to handle collisions      | 
|x-------------------------------------------------------x| 
| Date:                                         June 2015 | 
| Author:                              Andrius Jefremovas | 
| Description:      Axis aligned bounding box rigid body. | 
\--------------------------------------------------------*/

#ifndef RIGID_AABB_H
#define RIGID_AABB_H

#include "RigidBody.h"

namespace rigid_body_sys
{
    struct RigidAABB : public RigidBody
    {
        AABB m_aabb;  // Rigid AABB collision shape
        /* RigidAABB: position, orientation, linear and angular velocity,
                      half extents, inverse mass, coeficients of restitution
                      and friction, graphcis resources.*/
        RigidAABB(const glm::vec3 &pos,      const glm::vec3 &orient, 
                  const glm::vec3 &linVel,   const glm::vec3 &angVel,
                  const glm::vec3 &hExtents, const float invmass,     
                  const float restitution,   const float friction,    
                  GraphicsResouces *gr);
        // Semi-implicit Euler integration
        void Integrate(const float timeStep);
        // Calls integrate every physics update and centers AABB
        void Update(const float timeStep);
        // Renders a axis aligned bounding box
        void Render(const float time, 
                    const graphics_framework::camera *cam);
        // Computes acceleration given a force and applies it
        void AddForce(const glm::vec3 &force);
        // Test if AABB is colliding another object
        bool TestCollision(RigidBody &body, CollisionData &cd);
        bool TestCollision(RigidBody &body, OBB &obb, CollisionData &cd);
        bool TestCollision(RigidBody &body, AABB &aabb, CollisionData &cd);
        bool TestCollision(RigidBody &body, Sphere &sphere, CollisionData &cd);
        // Separate case for rigidplane
        bool TestCollision(RigidPlane &plane, CollisionData &cd);
    };
}; /* !rigid_body_sys */

#endif /* !RIGID_AABB_H */