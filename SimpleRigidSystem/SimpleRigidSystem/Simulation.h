/*---------------------------------------------------------\
| File: Simulation.h                                       |
|x--------------------------------------------------------x|
| Details: Initiates rigid bodies in various layouts, tests|
| their interaction collision properties and renders them. |
| Collisions are detected using overlap tests, if a colli- |
| sion is detected the collision point, penetration, normal|
| and applies an impulse.                                  |
|x--------------------------------------------------------x|
| Date:                                          June 2015 |
| Author:                               Andrius Jefremovas |
| Description:             Simulates rigid body collisions |
\---------------------------------------------------------*/
#ifndef SIMULATION_H
#define SIMULATION_H

#include "InputHandler.h"
#include "RigidBody.h"
#include "RigidPlane.h"
#include "RigidSphere.h"
#include "RigidAABB.h"
#include "RigidOBB.h"

namespace rigid_body_sys
{
    class Simulation
    {
    public:
        Simulation();
        ~Simulation();
        // Input call 1:1 per frame
        void Input (const float deltatime, InputHandler *ih);
        // Physics call N times a frame
        void Update(const float timeStep);
        // Render call
        void Render(const float totalTime,
                    const graphics_framework::camera *cam);
    private:
        bool m_simulate;                         // Simulation state
        std::vector<RigidBody*> m_rigidBodies;   // Collection of rigid bodies
        std::vector<RigidPlane*> m_planeBuffer;  // Collection of planes
        // Rendering properties: shapes, shaders and textures etc
        std::map<GraphicsSet, GraphicsResouces> m_graphics;
        std::vector<glm::vec3> dbgPoints;
        // Build meshes, shaders, textures... for rigid bodies
        void LoadGraphicResources();
    };
}; /* !rigid_body_sys */

#endif /* !RIGID_BODY_H */