/*--------------------------------------------------------\
| File: ColliderTypes.h                                   |
|x-------------------------------------------------------x|
| Details: Contains structures of basic bounding volumes  |
| and primitive types for collision detection.            |
|x-------------------------------------------------------x|  
| Date:                                         June 2015 |  
| Author:                              Andrius Jefremovas |  
| Description:       Definetions of bounding volume types.|  
\--------------------------------------------------------*/
#ifndef COLLIDER_TYPES_H
#define COLLIDER_TYPES_H

#include <graphics_framework.h>

namespace rigid_body_sys
{
    struct Segment
    {
        glm::vec3 start;
        glm::vec3 end;
        // Line segments: start point and end point
        Segment(const glm::vec3 s, const glm::vec3 e) : start(s), end(e) { }
    };

    struct Polygon
    {
        glm::vec3* vertices;    // Array of vertices
        int numVert;            // Number of vertices in array
    };

    struct Plane
    {
        glm::vec3 position;     // Position of plane
        glm::vec3 normal;       // Normal of plane
        Plane(const glm::vec3 &p, const glm::vec3 &n) : 
              position(p), normal(n) { };
    };

    struct AABB                  // Axis aligned bounding box
    {
        glm::vec3 position;      // Position/Center of AABB
        glm::vec3 hExtent;       // HalfExtents for each axis;
        AABB(const glm::vec3 &p, const glm::vec3 &r) : 
             position(p), hExtent(r) { };
    };

    struct OBB                   // Oriented bounding box
    {
        glm::vec3 position;      // Position/Center of OBB
        glm::vec3 hExtent;       // HalfExtents for each axis;
        glm::vec3 uAxis[3];      // Local axis x y z
        /* Oriented bounding box:
           p - center of OBB
           r - half extents
           u - x y z unit axis */
        OBB(const glm::vec3 &p, const glm::vec3 &r) :
            position(p), hExtent(r) { };
    };

    struct Rect
    {
        glm::vec3 position;      // Position/Center of Rectangle
        glm::vec2 halfExtent;    // HalfExtents for each axis;
        glm::vec3 uAxis[2];      // Local axis x y 
    };

    struct Sphere
    {
        glm::vec3 position;     // Position/Center of Sphere
        float radius;           // Radius of sphere
        Sphere(const glm::vec3 &p, const float r) : position(p), radius(r) { };
    };

    // REGION: R = { X | (x - [a + (b - a)*t])^2 <= r}, 0 <= t <= 1;
    struct Capsule
    {
        glm::vec3 start;        // Start of capsule segment
        glm::vec3 end;          // End of capsule segment
        float radius;           // Radius of capsule
    };

    struct Cylinder
    {
        glm::vec3 start;        // Start of capsule segment
        glm::vec3 end;          // End of capsule segment
        float radius;           // Radius of capsule
    };

    // REGION: R = { x | (x - [a + u[0]*s + u[1]*t]^2 <= r}, 0 <= s, t <= 1
    struct Lozenge
    {
        glm::vec3 position;     // Position/Center of Sphere
        glm::vec3 uAxis[2];     // Two edges axes of the rectangle
        float radius;           // Radius of Lozenge
    };
}; /* !rigid_body_sys */

#endif /* !COLLIDER_TYPES_H */