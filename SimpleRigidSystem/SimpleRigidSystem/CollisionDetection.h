/*--------------------------------------------------------\
| File: CollisionDetection.h                              |    
|x-------------------------------------------------------x|    
| Details: Contains collision detection algorithms for 5  |    
| different bounding volumes types. For each object there |    
| two function: the first one return whether two objects  |    
| are intersecting, the second also return collision data:|
| point, normal and penetration depth. The implementation |
| some of these algorithms follow Christer Ericson's book:|
| Real-Time Collision Detection.                          |
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:      Detects whether two objects intersect.|
\--------------------------------------------------------*/  

// Algorithms follow Christer Ericson's book: Real - Time Collision Detection.

#ifndef COLLISION_DETECTION_H
#define COLLISION_DETECTION_H

#include "SupportFunctions.h"

namespace rigid_body_sys
{

struct CollisionData
{
    float penetration; // Penetration depth
    glm::vec3 normal;  // Collision normal
    glm::vec3 point;   // Collision point
    CollisionData() = default;
    CollisionData(const float penetration,
                  const glm::vec3 normal,
                  const glm::vec3 point) :
                  penetration(penetration),
                  normal(normal),
                  point(point) { /* */ }
};

/*------------------------------------------------------------------------------
                            Primitives
------------------------------------------------------------------------------*/

// Returns true if a point is behind a plane and the distance
inline bool PointPlane(const glm::vec3 &point, const Plane &plane,
                       CollisionData &cd)
{
    float d = DistancePointPlane(point, plane);
    if (d < 0.0f)
    {
        cd.penetration = -d;
        cd.normal = plane.normal;
        cd.point = point;
        //cd.point = ClosestPointPlane(point, plane);
        return true;
    }
    return false;
}

/*------------------------------------------------------------------------------
                    Sphere volume collision detection
------------------------------------------------------------------------------*/
                                                                                
// Determines if a point is inside a sphere                                     
inline bool PointInSphere(const glm::vec3 &point, const Sphere &sphere)                
{                                                                               
    // Square distance between point and sphere center                          
    glm::vec3 dist(point - sphere.position);                                    
    float dist2 = glm::dot(dist, dist); // Avoids SQRT                                                       
    // Inside of sphere if squared distance is less then squared radius         
    return (dist2 <= sphere.radius*sphere.radius);                              
}                                                                               
                                                                                
// Determines if a sphere is intersecting a plane                               
inline bool SpherePlane(const Sphere &sphere, const Plane &plane)                      
{                                                                               
    float dist = glm::dot(sphere.position - plane.position, plane.normal);      
    return (abs(dist) <= sphere.radius);                                        
}                                                                               
                                                                                
/*  Determines if a sphere is intersecting a plane and returns:                 
    collision point, normal and penetration depth*/                             
inline bool SpherePlane(const Sphere &sphere, const Plane &plane,
                        CollisionData &cd)   
{                                                                               
    // Find the closest point on the plane to the sphere                        
    glm::vec3 closestPoint = ClosestPointPlane(sphere.position, plane);         
    // Sphere and plane intersect if sqDistance from sphere                     
    // to the closest point is less than the sqRadius                           
    glm::vec3 dist = sphere.position - closestPoint;
    float sqDist = glm::dot(dist, dist);    
    float sqRadius = sphere.radius*sphere.radius;

    if (sqDist <= sqRadius)
    {                                                                           
        cd.penetration = sqRadius - sqDist; 
        //cd.penetration = sphere.radius - sqrtf(sqDist);
        if (dist == glm::vec3(0.0f))
            dist.y -= glm::epsilon<float>();
        cd.normal = plane.normal; 
        //cd.normal = glm::normalize(dist);
        cd.point = closestPoint;
        return true;                                                            
    }                                                                           
    return false;                                                               
}                                                                               
                                                                                
// Determines if a sphere is intersecting another sphere                        
inline bool SphereSphere(const Sphere &a, const Sphere &b)                             
{                                                                               
    // Square distance between centres and sum of radii                         
    glm::vec3 dist = a.position - b.position;                                   
    float sqDist = glm::dot(dist, dist);                                        
    float radiiSum = a.radius + b.radius;                                                                                                       
    // Intersects if squared distance is less than the squared sum of radii     
    return (sqDist <= radiiSum*radiiSum);                                       
}                                                                               
                                                                                
/*  Determines if a sphere is intersecting another sphere and returns:          
    collision point, normal and penetration depth*/                             
inline bool SphereSphere(const Sphere &a, const Sphere &b, CollisionData &cd)          
{     
    // Square distance between centres and sum of radii                         
    glm::vec3 dist = a.position - b.position;
    float sqDist = glm::dot(dist, dist);
    float sqRadiiSum = a.radius + b.radius; 
    sqRadiiSum *= sqRadiiSum;
    // Intersects if squared distance is less than the squared sum of radii     
    if (sqDist <= sqRadiiSum)
    {                                                                           
        // Return collision data                                                
        cd.penetration = sqRadiiSum - sqDist;
        if (dist == glm::vec3(0.0f))
            dist.y -= glm::epsilon<float>();
        cd.normal = glm::normalize(dist);                                       
        cd.point = a.position - cd.normal *                                     
                  (a.radius - cd.penetration * 0.5f);                           
        return true;                                                            
    }                                                                           
    return false;                                                               
}                                                                               
                                                                                
// Determines if a sphere is intersecting a axis aligned bounding box           
inline bool SphereAABB(const Sphere &sphere, const AABB &aabb)                         
{                                                                               
    // Compute squared distance between sphere and AABB                         
    float sqDist = SqDistancePointAABB(sphere.position, aabb);                  
    // Sphere intersects AABB if the squared distance between them              
    // is less than the squared radius of the sphere                            
    return (sqDist <= sphere.radius*sphere.radius);                             
}                                                                               
                                                                                
/*  Determines if a sphere is intersecting a axis aligned bounding box          
    and returns: collision point, normal and penetration depth*/                
inline bool SphereAABB(const Sphere &sphere, const AABB &aabb, CollisionData &cd)      
{                                                                               
    // Find the closest point on AABB to the sphere                             
    glm::vec3 closestPoint = ClosestPointAABB(sphere.position, aabb);           
    // Sphere intersects AABB if the squared distance between them              
    // is less than the squared radius of the sphere                            
    glm::vec3 dist = sphere.position - closestPoint;
    float sqDist = glm::dot(dist, dist);
    float sqRadius = sphere.radius*sphere.radius;                                                                      
    if (sqDist <= sqRadius)
    {      
        cd.penetration = sqRadius - sqDist;
        if (dist == glm::vec3(0.0f))
            dist.y -= glm::epsilon<float>();
        cd.normal = glm::normalize(dist);                         
        cd.point = closestPoint;
        return true;                                                            
    }                                                                           
    return false;                                                               
}                                                                               
                                                                                
// Determines if a sphere is intersecting a oriented bounding box               
inline bool SphereOBB(const Sphere &sphere, const OBB &obb)                            
{                                                                               
    // Compute squared distance between Sphere and OBB                          
    float sqDist = SqDistancePointOBB(sphere.position, obb);                    
    return (sqDist <= sphere.radius*sphere.radius);                             
}                                                                               
                                                                                
/*  Determines if a sphere is intersecting a oriented bounding box              
    and returns: collision point, normal and penetration depth*/                
inline bool SphereOBB(const Sphere &sphere, const OBB &obb, CollisionData &cd)         
{                                                                               
    // Find closest point on OBB to the sphere                                  
    glm::vec3 closestPoint = ClosestPointOBB(sphere.position, obb);             
    // Sphere intersects OBB if the squared distance between them               
    // is less than the squared radius of the sphere                            
    glm::vec3 dist = closestPoint - sphere.position;
    float sqDist = glm::dot(dist, dist);   
    float sqRadius = sphere.radius*sphere.radius;

    if (sqDist <= sqRadius)
    {                                                                           
        cd.penetration = sqRadius - sqDist;
        //cd.penetration = sphere.radius - sqrtf(sqDist);  
        if (dist == glm::vec3(0.0f))
            dist.y -= glm::epsilon<float>();
        cd.normal = glm::normalize(dist);
        cd.point = closestPoint;
        return true;                                                            
    }                                                                           
    return false;                                                               
}                                                                               
                                                                                
// Determines if a sphere is intersecting a capsule                             
inline bool SphereCapsule(const Sphere &sphere, const Capsule &capsule)                
{
    // Compute squared distance between sphere and capsule
    float sqDist = SqDistancePointSegment(sphere.position, 
                                          capsule.start, capsule.end);
    // Intersection if squared distnace is smaller than squared sum of radii 
    float sumRadii = sphere.radius + capsule.radius;
    return (sqDist <=  sumRadii*sumRadii);
}

/*  Determines if a sphere is intersecting a capsule and returns:
    collision point, normal and penetration depth*/
inline bool SphereCapsule(const Sphere &sphere, const Capsule &capsule,
                          CollisionData &cd)
{
    float t = 0.0f;
    // Find closest point on capsule segment to the sphere
    glm::vec3 closestPoint = ClosestPointSegment(sphere.position, 
                                                 capsule.start,
                                                 capsule.end, t);
    glm::vec3 dist = closestPoint - sphere.position;
    float sqDist = glm::dot(dist, dist);
    float radiiSum = sphere.radius + capsule.radius;

    // Sphere intersects capsule if the squared distance between them
    // is less than the squared radius of their radii
    if (sqDist <= radiiSum*radiiSum)
    {
        cd.penetration = radiiSum - sqrtf(sqDist);
        if (dist == glm::vec3(0.0f))
            dist.y = -glm::epsilon<float>();
        cd.normal = glm::normalize(dist);
        cd.point = closestPoint - cd.normal * 
                   (capsule.radius - cd.penetration * 0.5f);
        return true;
    }
    return false;
}

/*------------------------------------------------------------------------------
            Axis Aligned Bounding box volume collision detection
------------------------------------------------------------------------------*/

// Determines if a axis aligned bounding box is intersecting a plane
inline bool AABBPlane(const AABB &aabb, const Plane &plane)
{
    // Compute the projection interval radius
    float r = glm::dot(aabb.hExtent, glm::abs(plane.normal));
    // Compute distance of box center from plane
    float dist = glm::dot(plane.normal, aabb.position - plane.position);
    // Intersection occurs when distance falls within [-r,+r] interval
    return (glm::abs(dist) <= r);
}

/*  Determines if a axis aligned bounding box is intersecting a plane
    and returns: collision point, normal and penetration depth*/
inline bool AABBPlane(const AABB &aabb, const Plane &plane, CollisionData &cd)
{
    // Compute the projection interval radius
    float r = glm::dot(aabb.hExtent, glm::abs(plane.normal));
    // Compute distance of box center from plane
    float dist = glm::dot(plane.normal, aabb.position - plane.position);
    // Intersection occurs when distance falls within [-r,+r] interval
    if (glm::abs(dist) <= r)
    {
        cd.penetration = r - dist;
        cd.normal = plane.normal;
        cd.point = ClosestPointPlane(aabb.position, plane);
        return true;
    }
    return false;
}

// Determines if a axis aligned bounding box is intersecting another AABB
inline bool AABBAABB(const AABB &a, const AABB &b)
{
    // Intersection occurs if the abs distances is less than all half extents
    bool x = (glm::abs(a.position.x-b.position.x) < (a.hExtent.x+b.hExtent.x));
    bool y = (glm::abs(a.position.y-b.position.y) < (a.hExtent.y+b.hExtent.y));
    bool z = (glm::abs(a.position.z-b.position.z) < (a.hExtent.z+b.hExtent.z));
    return (x && y && z);
}

/*  Determines if a axis aligned bounding box is intersecting another AABB
    and returns: collision point, normal and penetration depth*/
inline bool AABBAABB(const AABB &a, const AABB &b, CollisionData &cd)
{
    // Intersection occurs if the absolute distances is less than all half extents
    if(glm::abs(a.position.x - b.position.x) > (a.hExtent.x + b.hExtent.x)) return false;
    if(glm::abs(a.position.y - b.position.y) > (a.hExtent.y + b.hExtent.y)) return false;
    if(glm::abs(a.position.z - b.position.z) > (a.hExtent.z + b.hExtent.z)) return false;
    {
        cd.point = ClosestPointAABB(a.position, b);
        glm::vec3 dist = a.position - cd.point;
        if (dist == glm::vec3(0.0f))
            dist.y = -glm::epsilon<float>();
        cd.normal = glm::normalize(dist);
        cd.penetration = glm::dot(a.hExtent - glm::abs(dist), glm::abs(cd.normal));
    }
    return true;
}

inline bool AABBCapsule(const AABB &aabb, const Capsule &capsule)
{

    return true;
}

inline bool AABBCapsule(const AABB &aabb, const Capsule &capsule, CollisionData &cd)
{
    return false;
}

/*------------------------------------------------------------------------------
           Oriented Bounding box volume collision detection
------------------------------------------------------------------------------*/

// Determines if a oriented bounding box is intersecting a plane
inline bool OBBPlane(const OBB &obb, const Plane &plane)
{
    // Compute the projection interval radius
    float r = obb.hExtent.x * glm::abs(glm::dot(plane.normal, obb.uAxis[0])) +
              obb.hExtent.y * glm::abs(glm::dot(plane.normal, obb.uAxis[1])) +
              obb.hExtent.z * glm::abs(glm::dot(plane.normal, obb.uAxis[2]));
    // Compute the distance of box center from the plane
    float dist = glm::dot(plane.normal, obb.position - plane.position);
    // Intersection occurs when distance falls withing [-r, +r] interval
    return (abs(dist) <= r);
}

/*  Determines if a oriented bounding box is intersecting a plane
    and returns: collision point, normal and penetration depth*/
inline bool OBBPlane(const OBB &obb, const Plane &plane, CollisionData &cd)
{
    // Compute the projection interval radius
    float r = obb.hExtent.x * glm::abs(glm::dot(plane.normal, obb.uAxis[0])) +
              obb.hExtent.y * glm::abs(glm::dot(plane.normal, obb.uAxis[1])) +
              obb.hExtent.z * glm::abs(glm::dot(plane.normal, obb.uAxis[2]));
    // Compute distance of box center from plane
    float dist = glm::dot(plane.normal, obb.position - plane.position);
    // Intersection occurs when distance falls within [-r,+r] interval
    if (glm::abs(dist) <= r)
    {
        cd.penetration = r - dist;
        //cd.point = ClosestPointPlane(obb.position, plane);
        cd.point = ClosestPointOBB(ClosestPointPlane(obb.position, plane), obb);
        cd.normal = plane.normal; // glm::normalize(cd.point - obb.position);
        return true;
    }
    return false;
}

// Determines if a oriented bounding box is intersecting another OBB
inline bool OBBOBB(const OBB &a, const OBB &b)
{
    float ra, rb;
    glm::mat3x3 R, absR;
    // Compute rotation matrix expressing b in a's coordinate frame
    for (glm::uint i = 0; i < 3; i++)
    for (glm::uint j = 0; j < 3; j++)
        R[i][j] = glm::dot(a.uAxis[i], b.uAxis[j]);
    // Compute translation vector t
    glm::vec3 t = b.position - a.position;
    // Bring translation into a's frame
    t = glm::vec3(glm::dot(t, a.uAxis[0]), 
                  glm::dot(t, a.uAxis[1]),
                  glm::dot(t, a.uAxis[2]));
    // Compute common subexpressions. Add in epsilon to counteract arithmetic
    // errors when two edges are parallel and their cross product is near null
    for (glm::uint i = 0; i < 3; i++)
    for (glm::uint j = 0; j < 3; j++)
        absR[i][j] = glm::abs(R[i][j]) + glm::epsilon<float>();
    // Test 3 major axes of OBB a
    for (glm::uint i = 0; i < 3; i++)
    {
        ra = a.hExtent[i];
        rb = b.hExtent.x * absR[0][i] +
             b.hExtent.y * absR[1][i] +
             b.hExtent.z * absR[2][i];
        if (glm::abs(t[i]) > ra + rb)
            return false;
    }
    // Test 3 major axes of OBB b
    for (glm::uint i = 0; i < 3; i++)
    {
        ra = a.hExtent.x * absR[0][i] +
             a.hExtent.y * absR[1][i] +
             a.hExtent.z * absR[2][i];
        rb = b.hExtent[i];
        if (glm::abs(t.x * R[0][i] + t.y * R[1][i] + t.z * R[2][i]) > ra + rb)
            return false;
    }
    /* Test the 9 cross-axes */
    // A.x >< B.x
    ra = a.hExtent.y * absR[2][0] + a.hExtent.z * absR[1][0];
    rb = b.hExtent.y * absR[0][2] + b.hExtent.z * absR[0][1];
    if (glm::abs(t.z * R[1][0] - t.y * R[2][0]) > ra + rb)
        return false;
    // A.x >< B.y
    ra = a.hExtent.y * absR[2][1] + a.hExtent.z * absR[1][1];
    rb = b.hExtent.x * absR[0][2] + b.hExtent.z * absR[0][0];
    if (glm::abs(t.z * R[1][1] - t.y * R[2][1]) > ra + rb)
        return false;
    // A.x >< B.z
    ra = a.hExtent.y * absR[2][2] + a.hExtent.z * absR[1][2];
    rb = b.hExtent.x * absR[0][1] + b.hExtent.y * absR[0][0];
    if (glm::abs(t.z * R[1][2] - t.y * R[2][2]) > ra + rb)
        return false;
    // A.y >< B.x
    ra = a.hExtent.x * absR[2][0] + a.hExtent.z * absR[0][0];
    rb = b.hExtent.y * absR[1][2] + b.hExtent.z * absR[1][1];
    if (glm::abs(t.x * R[2][0] - t.z * R[0][0]) > ra + rb)
        return false;
    // A.y >< B.y
    ra = a.hExtent.x * absR[2][1] + a.hExtent.z * absR[0][1];
    rb = b.hExtent.x * absR[1][2] + b.hExtent.z * absR[1][0];
    if (glm::abs(t.x * R[2][1] - t.z * R[0][1]) > ra + rb)
        return false;
    // A.y >< B.z
    ra = a.hExtent.x * absR[2][2] + a.hExtent.z * absR[0][2];
    rb = b.hExtent.x * absR[1][1] + b.hExtent.y * absR[1][0];
    if (glm::abs(t.x * R[2][2] - t.z * R[0][2]) > ra + rb)
        return false;
    // A.z >< B.x
    ra = a.hExtent.x * absR[1][0] + a.hExtent.y * absR[0][0];
    rb = b.hExtent.y * absR[2][2] + b.hExtent.z * absR[2][1];
    if (glm::abs(t.y * R[0][0] - t.x * R[1][0]) > ra + rb)
        return false;
    // A.z >< B.y
    ra = a.hExtent.x * absR[1][1] + a.hExtent.y * absR[0][1];
    rb = b.hExtent.x * absR[2][2] + b.hExtent.z * absR[2][0];
    if (glm::abs(t.y * R[0][1] - t.x * R[1][1]) > ra + rb)
        return false;
    // A.z >< B.z
    ra = a.hExtent.x * absR[1][2] + a.hExtent.y * absR[0][2];
    rb = b.hExtent.x * absR[2][1] + b.hExtent.y * absR[2][0];
    if (glm::abs(t.y * R[0][2] - t.x * R[1][2]) > ra + rb)
        return false;
    // No separating axis found, thus the OBBs are intersecting
    return true;
}

/*  Determines if a oriented bounding box is intersecting another OBB
    and returns: collision point, normal and penetration depth*/
inline bool OBBOBB(const OBB &a, const OBB &b, CollisionData &cd)
{
    if (OBBOBB(a, b))
    {
        cd.point = ClosestPointOBB(a.position, b);
        glm::vec3 dist = cd.point - a.position;
        if (dist == glm::vec3(0.0f))
            dist.y = -glm::epsilon<float>();
        cd.normal = glm::normalize(dist);
        cd.penetration = glm::dot(a.hExtent - glm::abs(dist), glm::abs(cd.normal));
        return true;
    }
    return false;
}

// Determines if a oriented box is intersecting a axis aligned box
inline bool OBBAABB(const OBB &obb, const AABB &aabb)
{
    return true;
}

inline bool OBBAABB(const OBB &obb, const AABB &aabb, CollisionData &cd)
{
    OBB tempOBB = OBB(aabb.position, aabb.hExtent);
    tempOBB.uAxis[0] = glm::vec3(1.0f, 0.0f, 0.0f);
    tempOBB.uAxis[1] = glm::vec3(0.0f, 1.0f, 0.0f);
    tempOBB.uAxis[2] = glm::vec3(0.0f, 0.0f, 1.0f);

    if (OBBOBB(tempOBB, obb))
    {
        cd.point = ClosestPointOBB(aabb.position, obb);
        glm::vec3 dist = cd.point - aabb.position;
        if (dist == glm::vec3(0.0f))
            dist.y = -glm::epsilon<float>();
        cd.normal = glm::normalize(dist);
        cd.penetration = glm::dot(aabb.hExtent - glm::abs(dist), 
                                  glm::abs(cd.normal));
        return true;
    }
    return false;
}

// Determines if a oriented bounding box is intersecting a capsule
inline bool OBBCapsule(const OBB &obb, const Capsule &capsule)
{
    
    return false;
}

/*  Determines if a oriented bounding box is intersecting a capsule
    and returns: collision point, normal and penetration depth*/
inline bool OBBCapsule(const OBB &obb, const OBB &capsule, CollisionData &cd)
{

    return false;
}

/*------------------------------------------------------------------------------
                Capsule volume collision detection
------------------------------------------------------------------------------*/

// Determines if a capsule is intersecting a plane
inline bool CapsulePlane(const Capsule &capsule, const Plane &plane)
{
    return false;
}

/*  Determines if capsule c is intersecting plane p and returns:
    collision point, normal and penetration depth*/
inline bool CapsulePlane(const Capsule &c, const Plane &p, CollisionData &cd)
{
    return false;
}

// Determines if capsule a is intersecting a capsule b
inline bool CapsuleCapsule(const Capsule &a, const Capsule &b)
{
    return false;
}

/*  Determines if capsule a is intersecting capsule b and returns:
    collision point, normal and penetration depth*/
inline bool CapsuleCapsule(const Capsule &a, const Capsule &b, CollisionData &cd)
{
    return false;
}

}; /* !rigid_body_sys */
#endif /* !COLLISION_DETECTION_H */