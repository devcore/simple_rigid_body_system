/*--------------------------------------------------------\
| File: InputeHandler.h                                   |
|x-------------------------------------------------------x|
| Details: The InputHandler creates cameras and processes |
| input to translate and rotate them in the world. It also|
| updates ImGUI with the current mouse position and state.|
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:          Create cameras and updates ImGUI. |
\--------------------------------------------------------*/
#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include "Util.h"

namespace rigid_body_sys
{
    struct InputHandler
    {
        InputHandler();
        ~InputHandler();
        // Camera setups
        void InitFreeCam();
        void InitArcCam();
        // Controls for using cameras
        void ProcessInput(const float deltaTime);
        // Update methods for different camera types
        void UpdateFreeCam(const float deltaTime);
        void UpdateArcCam(const float deltaTime);
        // Use CameraSet enum to select a camera type
        void SetCurrentCam(const CameraSet &cam);
        graphics_framework::camera* GetCurrentCam();
    private:
        CameraSet m_currentCam;
        std::map<CameraSet, graphics_framework::camera*> m_cameras;
        bool m_enableMouse;                  // Mouse capture enable/disable
        float m_aspect;                      // Screen aspect ratio
        // Parameters for mouse movement to camera rotation
        double m_cursorX,    m_cursorY;      // X-Y cursor position             
        double m_currentX,   m_currentY;     // X-Y current cursor position
        double m_deltaX,     m_deltaY;       // X-Y amount the cursor moved
        double m_ratioWidth, m_ratioHeight;  // Screen width-height ratio             
    };
}; /* !rigid_body_sys */

#endif /* !INPUT_HANDLER_H */