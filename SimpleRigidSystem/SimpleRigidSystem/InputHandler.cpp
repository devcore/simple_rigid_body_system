/*
File: InputHandler.cpp
--------------------------------------------------------------------------------
For details see InputHandler.h
*/
#include "InputHandler.h"
//#include "ImGui\imgui.h" TODO: Figure out what causes stuttering

using namespace glm;
using namespace graphics_framework;
using namespace rigid_body_sys;

InputHandler::InputHandler()
{
    // Default values
    m_enableMouse = true;
    m_currentCam = e_FREE_CAM;
    // Calculate aspect and screen ratios
    m_aspect = static_cast<float>(renderer::get_screen_width()) / 
               static_cast<float>(renderer::get_screen_height());
    m_ratioWidth = quarter_pi<float>() / 
                   static_cast<float>(renderer::get_screen_width());
    m_ratioHeight = (quarter_pi<float>() *
                    (static_cast<float>(renderer::get_screen_height()) /
                    static_cast<float>(renderer::get_screen_width()))) /
                    static_cast<float>(renderer::get_screen_height());
    // Initialise cursor position at zero
    m_cursorX = m_cursorY = 0.0;
    m_deltaX = m_deltaY = 0.0;
    // Hide cursor arrow
    glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    // Get current position of cursor
    glfwGetCursorPos(renderer::get_window(), &m_cursorX, &m_cursorY);
    // Initilise free camera
    InitFreeCam();
    // Initialise arc ball camera
    InitArcCam();
}

InputHandler::~InputHandler()
{
    for (auto cam : m_cameras)
    {
        delete m_cameras[cam.first];
    }
    //delete m_cameras[e_FREE_CAM];
    //delete m_cameras[e_ARC_CAM];
    std::cerr << "LOG - Cameras destroyed" << std::endl;
}

void InputHandler::InitFreeCam()
{
    // Free cam initilisation
    free_camera* cam = new free_camera();
    cam->set_position(vec3(10.0f));
    cam->set_target(vec3(0.0f));
    // Set field of view to ~80 degrees, aspect, near plane, far plane 
    cam->set_projection(pi<float>()*0.45f, m_aspect, 2.414f, 1000.0f);
    m_cameras[e_FREE_CAM] = cam;
    std::cerr << "LOG - Free-camera initialized." << std::endl;
}

void InputHandler::InitArcCam()
{
    // Arc camera initilisation
    arc_ball_camera* cam = new arc_ball_camera();
    cam->set_position(vec3(0.0f));
    cam->set_target(vec3(5.0f));
    cam->set_distance(50.0f);
    cam->rotate(-quarter_pi<float>()*0.725f, pi<float>());
    // Set field of view to ~80 degrees, aspect, near plane, far plane 
    cam->set_projection(pi<float>()*0.45f, m_aspect, 2.414f, 10000.0f);
    m_cameras[e_ARC_CAM] = cam;
    glfwSetScrollCallback(renderer::get_window(), ScrollWheel);
    std::cerr << "LOG - Arc-camera initialized." << std::endl;
}

void InputHandler::ProcessInput(const float deltaTime)
{
    // Update the current camera
    switch (m_currentCam)
    {
    case e_FREE_CAM:
        UpdateFreeCam(deltaTime);
        break;
    case e_ARC_CAM:
        UpdateArcCam(deltaTime);
        break;
    default:
        std::cerr << "LOG - Camera type not selected." << std::endl;
        __debugbreak();
        break;
    }
    // Toggle cursor
    if (glfwGetKeyOnce(GLFW_KEY_LEFT_ALT))
    {
        if (m_enableMouse = !m_enableMouse)
        {
            glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
        else
        {
            glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
    }
    // Toggle current camera
    if (glfwGetKeyOnce(GLFW_KEY_TAB))
    {
        m_currentCam = (m_currentCam == e_FREE_CAM) ? e_ARC_CAM : e_FREE_CAM;
    }
    // Reset mouse movement
    m_deltaX = 0.0;
    m_deltaY = 0.0;
}

void InputHandler::UpdateFreeCam(const float deltaTime)
{
    // Get new cursor position
    glfwGetCursorPos(renderer::get_window(), &m_currentX, &m_currentY);
    // If the mouse is active - track its position
    if (m_enableMouse)
    {
        // Get the amount the cursor has moved
        m_deltaX = (m_currentX - m_cursorX) * m_ratioWidth;
        m_deltaY = (m_currentY - m_cursorY) * m_ratioHeight;
        // Keep the old position
        m_cursorX = m_currentX;
        m_cursorY = m_currentY;
    }
    // Camera rotation input 
    if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
        m_deltaX -= 2.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
        m_deltaX += 2.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
        m_deltaY -= 2.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
        m_deltaY += 2.0f * deltaTime;
    // Camera movement input
    vec3 translation(0.0f, 0.0f, 0.0f);
    if (glfwGetKey(renderer::get_window(), 'W'))
        translation.z += 10.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'S'))
        translation.z -= 10.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'A'))
        translation.x -= 10.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'D'))
        translation.x += 10.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'Q'))
        translation.y -= 10.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'E'))
        translation.y += 10.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT_SHIFT))
        translation *= 15.0f;
    // Update camera using the input
    free_camera* cam = static_cast<free_camera*>(m_cameras[e_FREE_CAM]);
    cam->rotate(static_cast<float>(m_deltaX), -static_cast<float>(m_deltaY));
    cam->move(translation);
    cam->update(deltaTime);
}

void InputHandler::UpdateArcCam(const float deltaTime)
{
    // Get new cursor position
    glfwGetCursorPos(renderer::get_window(), &m_currentX, &m_currentY);
    float translation{ 0.0f };
    // If the mouse is active - track its position
    if (m_enableMouse)
    {
        // Get the amount the cursor has moved
        m_deltaX = (m_currentX - m_cursorX) * m_ratioWidth;
        m_deltaY = (m_currentY - m_cursorY) * m_ratioHeight;
        // Keep the old position
        m_cursorX = m_currentX;
        m_cursorY = m_currentY;
        translation += scroll_wheel;
    }
    // Camera movement input
    if (glfwGetKey(renderer::get_window(), 'W'))
        m_deltaY -= 5.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'S'))
        m_deltaY += 5.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'A'))
        m_deltaX -= 5.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'D'))
        m_deltaX += 5.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'Q'))
        translation -= 2.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), 'E'))
        translation += 2.0f * deltaTime;
    if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT_SHIFT))
        translation *= 15.0f;
    // Update camera using the input
    arc_ball_camera* cam = static_cast<arc_ball_camera*>(m_cameras[e_ARC_CAM]);
    cam->rotate(static_cast<float>(m_deltaY), static_cast<float>(m_deltaX));
    cam->move(translation);
    cam->update(deltaTime);
    // Reset scroll wheel input
    scroll_wheel = 0.0f;
}

void InputHandler::SetCurrentCam(const CameraSet &cam)
{
    switch (cam)
    {
    case CameraSet::e_FREE_CAM:
        m_currentCam = CameraSet::e_FREE_CAM;
        break;
    case CameraSet::e_ARC_CAM:
        m_currentCam = CameraSet::e_ARC_CAM;
        break;
    default:
        std::cerr << "LOG - Uknown cam_type! or not setup!" << std::endl;
#if DEBUG | _DEBUG // Break in debug mode
        __debugbreak();
#endif
        // Set free cam as default and return it
        m_currentCam = CameraSet::e_FREE_CAM;
        break;
    }
}

camera* InputHandler::GetCurrentCam()
{
    return m_cameras[m_currentCam];
}