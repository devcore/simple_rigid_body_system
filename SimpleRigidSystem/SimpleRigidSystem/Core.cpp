/*
File: Core.cpp
--------------------------------------------------------------------------------
For details see Core.h
*/
#include "Core.h"

using namespace glm;
using namespace graphics_framework;
using namespace rigid_body_sys;

Core::Core(float timeStep, unsigned int tickRate) : 
    TIME_STEP(timeStep), TICK_RATE(1.0f/tickRate), m_accumTime(0.0f) { }

Core::~Core()
{
    delete m_skybox;
    delete m_simulation;
    delete m_inputHandler;
}

bool Core::Initialise()
{
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    // Initiliase the input handler and position the default camera
    m_inputHandler = new InputHandler();
    m_inputHandler->SetCurrentCam(CameraSet::e_ARC_CAM);
    arc_ball_camera* cam{ static_cast<arc_ball_camera*>(m_inputHandler->GetCurrentCam()) };
    cam->set_position(vec3(0.0f));
    cam->set_target(vec3(0.0f, 5.0f, 0.0f));
    cam->set_distance(120.0f);
    cam->set_rot_X(-pi<float>()*0.1f);
    cam->set_rot_Y(0.0f);

    // Create Rigidbody Simulation
    m_simulation = new Simulation();
    // Create Skybox
    m_skybox = new Skybox();
    return true;
}

bool Core::Update(const float deltaTime)
{
    // Process user input first
    Input(deltaTime);
    // Fixed update loop
    {
        // Clamp frame time
        float dt = glm::min(deltaTime, 0.25f);
        // Accumulate time passed
        m_accumTime += dt;
        // Update while enough time
        while (m_accumTime >= TICK_RATE)
        {
            m_simulation->Update(TIME_STEP);
            m_accumTime -= TICK_RATE;
        }
    }
    return true;
}

bool Core::Render(const float totalTime)
{
    m_skybox->Render(totalTime, m_inputHandler->GetCurrentCam());
    m_simulation->Render(totalTime, m_inputHandler->GetCurrentCam());
    return true;
}

void Core::Input(const float deltaTime)
{
    // Update cameras based on input
    m_inputHandler->ProcessInput(deltaTime);
    // Switch tests and pause/resume simulation
    m_simulation->Input(deltaTime, m_inputHandler);
}