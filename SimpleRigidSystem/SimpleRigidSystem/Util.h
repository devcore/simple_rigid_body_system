/*--------------------------------------------------------\
| File: Util.h                                            |
|x-------------------------------------------------------x|
| Details: Contains definitions of types, structs, macros |
| and commonly used headers files.                        |
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:     Generic utility types and definitions. |
\--------------------------------------------------------*/
#ifndef UTIL_H
#define UTIL_H

#include <graphics_framework.h>

namespace rigid_body_sys
{
    struct GraphicsResouces
    {
        graphics_framework::mesh mesh;
        graphics_framework::effect eff;
        graphics_framework::texture tex;
        graphics_framework::material mat;
        graphics_framework::directional_light light;
        GraphicsResouces(const graphics_framework::mesh &m,
                         const graphics_framework::effect &e,
                         const graphics_framework::texture &t,
                         const graphics_framework::material &mat,
                         const graphics_framework::directional_light &l) :
                         mesh(m), eff(e), tex(t), mat(mat), light(l) { }
        GraphicsResouces() = default;
    };

    enum GraphicsSet
    {
        e_PLANE,
        e_SPHERE,
        e_AABB,
        e_OBB,
        e_CAPSULE,
        e_DBG_PT
    };

    enum CameraSet
    {
        e_FREE_CAM,
        e_ARC_CAM,
        e_TARGET_CAM,
        e_CHASE_CAM
    };

    // GLFW does not offer access to a key press once or their table...
    static bool key_states[GLFW_KEY_LAST + 1];
    static float scroll_wheel = 0.0f;
    
    // Returns true when a key is first pressed.
    // -Usage: if(glfwGetKeyOnce('1')) { foo() };
    static bool glfwGetKeyOnce(int key)
    {
        if (glfwGetKey(graphics_framework::renderer::get_window(), key))
        {
            return (key_states[key] ? false : (key_states[key] = true));
        }
        return (key_states[key] = false);
    }
    static void ScrollWheel(GLFWwindow* window, double up, double down)
    {
        scroll_wheel = static_cast<float>(down)+0.01f;
    }
}; /* !rigid_body_sys */

#endif/* !RIGID_BODY_H */