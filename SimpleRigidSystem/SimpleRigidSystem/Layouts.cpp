/*
File: Layouts.cpp
--------------------------------------------------------------------------------
For details see Layouts.h
*/
#include "Layouts.h"

using namespace glm;
using namespace graphics_framework;

namespace rigid_body_sys
{

void LayoutDefault(std::vector<RigidBody*> &rigidBodies,
                   std::vector<RigidPlane*> &planeBuffer,
                   std::map<GraphicsSet, GraphicsResouces> &grMap)
{
    // Clean up the previous containers
    for (uint i = 0; i < rigidBodies.size(); i++)
    {
        delete rigidBodies[i];
    }
    rigidBodies.clear();
    for (uint i = 0; i < planeBuffer.size(); i++)
    {
        delete planeBuffer[i];
    }
    planeBuffer.clear();

    // Parameters
    vec3 pos{ 36.0f, 20.0f, 0.0f };
    const vec3 offset{ 36.0f, 0.0f, 0.0f };
    const float size{ 12.0f };
    const float bounce{ 0.5f };
    const float frict{ 0.5f };
    const float invmass{ 0.5f };

    // Create each body in line
    rigidBodies.reserve(3);
    // Create a rigid obb 
    rigidBodies.push_back(
        new RigidOBB(pos, vec3(quarter_pi<float>()), vec3(0.0f), vec3(0.0f),
                     vec3(size), invmass, bounce, frict, &grMap.at(e_OBB)));
    pos -= offset;
    // Create a rigid aabb
    rigidBodies.push_back(
        new RigidAABB(pos, vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(size),
                      invmass, bounce, frict, &grMap.at(e_AABB)));
    pos -= offset;
    // Create a rigid sphere
    rigidBodies.push_back(
        new RigidSphere(pos, vec3(-half_pi<float>(), 0, 0), vec3(0), vec3(0),
                        size, invmass, bounce, frict, &grMap.at(e_SPHERE)));
    // Create plane
    planeBuffer.resize(1);
    planeBuffer[0] = new RigidPlane(vec3(0.0f), vec3(0.0f, 1.0f, 0.0f),
                    bounce, frict, vec3(0.0f), vec3(20.0f), &grMap.at(e_PLANE));
}

void LayoutContactTest(std::vector<RigidBody*> &rigidBodies,
                       std::vector<RigidPlane*> &planeBuffer,
                       std::map<GraphicsSet, GraphicsResouces> &grMap)
{
    // Clean up the previous containers
    for (uint i = 0; i < rigidBodies.size(); i++)
    {
        delete rigidBodies[i];
    }
    rigidBodies.clear();
    for (uint i = 0; i < planeBuffer.size(); i++)
    {
        delete planeBuffer[i];
    }
    planeBuffer.clear();

    // Parameters
    vec3 pos{ 20.0f, 10.0f, 0.0f };
    const vec3 offset{ 20.0f, 0.0f, 0.0f };
    const vec3 vecZero{ 0.0f, 0.0f, 0.0f };
    const float size{ 10.0f };
    const float bounce{ 0.0f };
    const float frict{ 1.0f };
    const float invmass{ 0.25f };

    rigidBodies.reserve(9);
    // Create each body in line
    rigidBodies.push_back(
        new RigidOBB(pos, vecZero, vecZero, vecZero, vec3(size), invmass, 
                     bounce, frict, &grMap.at(e_OBB)));
    pos -= offset;
    rigidBodies.push_back(
        new RigidAABB(pos, vecZero, vecZero, vecZero, vec3(size), invmass, 
                      bounce, frict, &grMap.at(e_AABB)));
    pos -= offset;
    rigidBodies.push_back(
        new RigidSphere(pos, vec3(-half_pi<float>(), 0, 0), vecZero, vecZero,
                        size, invmass, bounce, frict, &grMap.at(e_SPHERE)));
    // Stack another line with different order
    pos = vec3(20.0f, 30.0f, 0.0f);
    rigidBodies.push_back(
        new RigidAABB(pos, vecZero, vecZero, vecZero, vec3(size), invmass, 
                      bounce, frict, &grMap.at(e_AABB)));
    pos -= offset;
    rigidBodies.push_back(
        new RigidSphere(pos, vec3(-half_pi<float>(), 0, 0), vecZero, vecZero,
                        size, invmass, bounce, frict, &grMap.at(e_SPHERE)));
    pos -= offset;
    rigidBodies.push_back(
        new RigidOBB(pos, vecZero, vecZero, vecZero, vec3(size), invmass,
                     bounce, frict, &grMap.at(e_OBB)));
    // Stack another line with different order
    pos = vec3(20.0f, 50.0f, 0.0f);
    rigidBodies.push_back(
        new RigidSphere(pos, vec3(-half_pi<float>(), 0, 0), vecZero, vecZero,
                        size, invmass, bounce, frict, &grMap.at(e_SPHERE)));
    pos -= offset;
    rigidBodies.push_back(
        new RigidOBB(pos, vecZero, vecZero, vecZero, vec3(size), invmass,
                     bounce, frict, &grMap.at(e_OBB)));
    pos -= offset;
    rigidBodies.push_back(
        new RigidAABB(pos, vecZero, vecZero, vecZero, vec3(size), invmass, 
                      bounce, frict, &grMap.at(e_AABB)));
    // Create plane
    planeBuffer.resize(1);
    planeBuffer[0] = new RigidPlane(vec3(0.0f), vec3(0.0f, 1.0f, 0.0f),
                    bounce, frict, vec3(0.0f), vec3(20.0f), &grMap.at(e_PLANE));
}

void LayoutSphereSmashTest(std::vector<RigidBody*> &rigidBodies,
                           std::vector<RigidPlane*> &planeBuffer,
                           std::map<GraphicsSet, GraphicsResouces> &grMap)
{
    // Clean up the previous containers
    for (uint i = 0; i < rigidBodies.size(); i++)
    {
        delete rigidBodies[i];
    }
    rigidBodies.clear();
    for (uint i = 0; i < planeBuffer.size(); i++)
    {
        delete planeBuffer[i];
    }
    planeBuffer.clear();

    // Generate a cube of sphere
    std::vector<vec3> posBuffer;
    vec3 pos = vec3(0.0f), size = vec3(15.0f), offset = vec3(6.0f);
    for (pos.x = -size.x; pos.x < size.x; pos.x += offset.x)
    for (pos.y = -size.y; pos.y < size.y; pos.y += offset.y)
    for (pos.z = -size.z; pos.z < size.z; pos.z += offset.z)
    {
        posBuffer.push_back(pos);
    }

    // Cull the cube to form a sphere and make three cases
    const float radius{ 3.0f }, invmass{ 0.2f };
    const float PI{ pi<float>() };
    const float hPI{ half_pi<float>() };
    for (uint i = 0; i < posBuffer.size(); i++)
    {
        if (length(posBuffer[i]) <= 12.0f)
        {
            // Most bouncy sphere at the left side
            posBuffer[i] += vec3(-60.0f, size.y + 25.0f, 0.0f);
            rigidBodies.push_back(
                        new RigidSphere(posBuffer[i], vec3(-hPI, 0.0f, 0.0f),
                        vec3(0.0f), vec3(0.0f), radius, invmass, 0.9f, 0.5f,
                        &grMap.at(e_SPHERE)));
            // Medium bouncy sphere at the centre
            posBuffer[i] += vec3(60.0f, 0.0f, 0.0f);
            rigidBodies.push_back(
                        new RigidSphere(posBuffer[i], vec3(-hPI, 0.0f, 0.0f),
                        vec3(0.0f), vec3(0.0f), radius, invmass, 0.6f, 0.5f,
                        &grMap.at(e_SPHERE)));
            // Non-bouncy sphere at the right side
            posBuffer[i] += vec3(60.0f, 0.0f, 0.0f);
            rigidBodies.push_back(
                        new RigidSphere(posBuffer[i], vec3(-hPI, 0.0f, 0.0f),
                        vec3(0.0f), vec3(0.0f), radius, invmass, 0.0f, 0.5f,
                        &grMap.at(e_SPHERE)));
        }
    }

    { /* Plane setup */
        const float size{ 50.0f }; // Purely visual, since planes are infinite..
        const float bounce{ 1.0f }; // coef of restitution
        const float friction{ 1.0f }; // coef of friction
        // Positions of planes
        const vec3 p[10]
        {
            vec3(       0.0f, 0.0f, 0.0f), vec3(      0.0f, size*2,  0.0f), // Y
            vec3(  size*2.0f, size, 0.0f), vec3(-size*2.0f,   size,  0.0f), // X
            vec3(       0.0f, size, size), vec3(      0.0f,   size, -size), // Z
            vec3( size*0.75f, size, 0.0f), vec3( size*0.8f,   size,  0.0f), // Mid
            vec3(-size*0.75f, size, 0.0f), vec3(-size*0.8f,   size,  0.0f), // Mid
        };
        // Normals of planes
        const vec3 n[10]
        {
            vec3( 0.0f, 1.0f,  0.0f), vec3( 0.0f, -1.0f, 0.0f),
            vec3(-1.0f, 0.0f,  0.0f), vec3( 1.0f,  0.0f, 0.0f),
            vec3( 0.0f, 0.0f, -1.0f), vec3( 0.0f,  0.0f, 1.0f),
            vec3(-1.0f, 0.0f,  0.0f), vec3( 1.0f,  0.0f, 0.0f),
            vec3( 1.0f, 0.0f,  0.0f), vec3(-1.0f,  0.0f, 0.0f)
        };
        float scale{ 9.0f };
        // Scale of planes
        const vec3 s[10]
        {
            vec3(scale*2.0f, scale, scale), vec3(scale*2.0f, scale, scale),
            vec3(scale,      scale, scale), vec3(scale,      scale, scale),
            vec3(scale*2.0f, scale, scale), vec3(scale*2.0f, scale, scale),
            vec3(scale,      scale, scale), vec3(scale,      scale, scale),
            vec3(scale,      scale, scale), vec3(scale,      scale, scale),
        };
        // Orientation of planes
        const vec3 o[10]
        {
            vec3(0.0f, 0.0f, 0.0f), vec3( PI, 0.0f, 0.0f),
            vec3( hPI, -hPI, 0.0f), vec3(hPI,  hPI, 0.0f),
            vec3(-hPI, 0.0f,   PI), vec3(hPI, 0.0f, 0.0f),
            vec3( hPI, -hPI, 0.0f), vec3(hPI,  hPI, 0.0f),
            vec3( hPI,  hPI, 0.0f), vec3(hPI, -hPI, 0.0f),
        };

        planeBuffer.resize(10);
        for (uint i = 0; i < 10; i++)
        {
            planeBuffer[i] = new RigidPlane(p[i], n[i], bounce, friction, 
                                            o[i], s[i], &grMap.at(e_PLANE));
        }
    } /* !Plane setup */
}

void LayoutMixWallTest(std::vector<RigidBody*> &rigidBodies,
                       std::vector<RigidPlane*> &planeBuffer,
                       std::map<GraphicsSet, GraphicsResouces> &grMap)
{
    // Clean up the previous containers
    for (uint i = 0; i < rigidBodies.size(); i++)
    {
        delete rigidBodies[i];
    }
    rigidBodies.clear();
    for (uint i = 0; i < planeBuffer.size(); i++)
    {
        delete planeBuffer[i];
    }
    planeBuffer.clear();
    const float radius{ 4.0f }, invmass{ 1.0f };
    const float PI{ pi<float>() }, hPI{ half_pi<float>() };
    float bounce{ 0.0f }, frict{ 1.0f }; // coef of restitution and friction
    const float offset{ radius*2.02f };
    rigidBodies.reserve(52);
    for (uint i = 0; i < 5; i++)
    {
        rigidBodies.push_back(
            new RigidAABB(vec3(46.0f, 4.0f, -16.0f + offset*i),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(radius), 
                    invmass, bounce, frict, &grMap.at(e_AABB)));
        rigidBodies.push_back(
                    new RigidSphere(vec3(46.0f, 12.0f, -16.0f + offset*i),
                    vec3(-hPI, 0.0f, 0.0f), vec3(0.0f), vec3(0.0f), radius,
                    invmass, bounce, frict, &grMap.at(e_SPHERE)));
        rigidBodies.push_back(
                    new RigidAABB(vec3(46.0f, 20.0f, -16.0f + offset*i),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(radius), 
                    invmass, bounce, frict, &grMap.at(e_AABB)));
        rigidBodies.push_back(
                    new RigidOBB(vec3(46.0f, 28.0f, -16.0f + offset*i),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(radius),
                    invmass, bounce, frict, &grMap.at(e_OBB)));
        rigidBodies.push_back(
                    new RigidSphere(vec3(46.0f, 36.0f, -16.0f + offset*i),
                    vec3(-hPI, 0.0f, 0.0f), vec3(0.0f), vec3(0.0f), radius,
                    invmass, bounce, frict, &grMap.at(e_SPHERE)));
        rigidBodies.push_back(
                    new RigidAABB(vec3(46.0f, 44.0f, -16.0f + offset*i), 
                    vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(radius), 
                    invmass, bounce, frict, &grMap.at(e_AABB)));
        rigidBodies.push_back(
                    new RigidSphere(vec3(46.0f, 52.0f, -16.0f + offset*i),
                    vec3(-hPI, 0.0f, 0.0f), vec3(0.0f), vec3(0.0f), radius,
                    invmass, bounce, frict, &grMap.at(e_SPHERE)));
    }
    for (uint i = 0; i < 7; i++)
    {
        rigidBodies.push_back(
                    new RigidAABB(vec3(46.0f, 4.0f + 8.0f*i, -24.1f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(radius),
                    invmass, bounce, frict, &grMap.at(e_AABB)));
        rigidBodies.push_back(
                    new RigidAABB(vec3(46.0f, 4.0f + 8.0f*i, 24.5f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(radius),
                    invmass, bounce, frict, &grMap.at(e_AABB)));
    }

    rigidBodies.push_back(
                new RigidSphere(vec3(-50.0f, 10.0f, 0.0f),
                                vec3(-hPI, 0.0f, 0.0f), 
                                vec3(120.0f, 40.0f, 0.0f), 
                                vec3(0.0f, 0.0f, -10.0f), 
                                10.0f, 0.2f, bounce, frict, 
                                &grMap.at(e_SPHERE)));
    rigidBodies.push_back(
                new RigidSphere(vec3(-50.0f, 8.0f, -24.0f),
                                vec3(-hPI, 0.0f, 0.0f),
                                vec3(80.0f, 5.0f, 0.0f),
                                vec3(0.0f, 0.0f, -10.0f),
                                8.0f, 0.4f, bounce, frict,
                                &grMap.at(e_SPHERE)));
    rigidBodies.push_back(
                new RigidSphere(vec3(-50.0f, 8.0f, 24.0f),
                                vec3(-hPI, 0.0f, 0.0f),
                                vec3(120.0f, 20.0f, 60.0f),
                                vec3(0.0f, 0.0f, -10.0f),
                                6.0f, 0.05f, 0.5f, frict,
                                &grMap.at(e_SPHERE)));

    { /* Plane setup */
        const float size{ 50.0f }; // Purely visual, since planes are infinite..
        bounce = 0.5f; frict = 1.0f;
        // Positions of planes
        const vec3 p[6]
        {
            vec3(     0,    0,    0), vec3(      0, size*2,    0), // Y
            vec3(size*2, size,    0), vec3(-size*2, size,      0), // X
            vec3(     0, size, size), vec3(      0, size,  -size), // Z
        };
        // Normals of planes
        const vec3 n[6]
        {
            vec3( 0.0f, 1.0f,  0.0f), vec3( 0.0f, -1.0f, 0.0f),
            vec3(-1.0f, 0.0f,  0.0f), vec3( 1.0f,  0.0f, 0.0f),
            vec3( 0.0f, 0.0f, -1.0f), vec3( 0.0f,  0.0f, 1.0f)
        };
        float scale{ 9.0f };
        // Scale of planes
        const vec3 s[6]
        {
            vec3(scale*2, scale, scale), vec3(scale*2, scale, scale),
            vec3(scale,   scale, scale), vec3(scale,   scale, scale),
            vec3(scale*2, scale, scale), vec3(scale*2, scale, scale)
        };
        // Orientation of planes
        const vec3 o[6]
        {
            vec3(0.0f, 0.0f, 0.0f), vec3( PI, 0.0f, 0.0f),
            vec3( hPI, -hPI, 0.0f), vec3(hPI,  hPI, 0.0f),
            vec3(-hPI, 0.0f,   PI), vec3(hPI, 0.0f, 0.0f)
        };

        planeBuffer.resize(6);
        for (uint i = 0; i < 6; i++)
        {
            planeBuffer[i] = new RigidPlane(p[i], n[i], bounce, frict,
                                            o[i], s[i], &grMap.at(e_PLANE));
        }
    } /* !Plane setup */
}

void LayoutFrictionTest(std::vector<RigidBody*> &rigidBodies,
                        std::vector<RigidPlane*> &planeBuffer,
                        std::map<GraphicsSet, GraphicsResouces> &grMap)
{
    // Clean up the previous containers
    for (uint i = 0; i < rigidBodies.size(); i++)
    {
        delete rigidBodies[i];
    }
    rigidBodies.clear();
    for (uint i = 0; i < planeBuffer.size(); i++)
    {
        delete planeBuffer[i];
    }
    planeBuffer.clear();
    // Attributes
    const float bounce = 0.0f, frict = 0.0f;
    const float PI{ pi<float>() };
    const float hPI{ half_pi<float>() };
    const float qPI{ quarter_pi<float>() };
    const float radius{ 4.0f }, invmass{ 0.1f };

    // Place 3 of each rigid body type on a slope
    rigidBodies.reserve(9);
    rigidBodies.push_back(
                    new RigidAABB(vec3(85.0f, 35.0f, 40.0f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f),
                    vec3(radius), invmass, bounce, 0.0f, &grMap.at(e_AABB)));
    rigidBodies.push_back(
                    new RigidAABB(vec3(85.0f, 35.0f, 30.0f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f),
                    vec3(radius), invmass, bounce, 0.5f, &grMap.at(e_AABB)));
    rigidBodies.push_back(
                    new RigidAABB(vec3(85.0f, 35.0f, 20.0f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f), 
                    vec3(radius), invmass, bounce, 2.0f, &grMap.at(e_AABB)));
    rigidBodies.push_back(
                    new RigidSphere(vec3(85.0f, 35.0f, 10.0f),
                    vec3(-hPI, 0.0f, 0.0f), vec3(0.0f), vec3(0.0f),
                    radius, invmass, bounce, 0.0f, &grMap.at(e_SPHERE)));
    rigidBodies.push_back(
                    new RigidSphere(vec3(85.0f, 35.0f, 0.0f),
                    vec3(-hPI, 0.0f, 0.0f), vec3(0.0f), vec3(0.0f),
                    radius, invmass, bounce, 0.5f, &grMap.at(e_SPHERE)));
    rigidBodies.push_back(
                    new RigidSphere(vec3(85.0f, 35.0f, -10.0f),
                    vec3(-hPI, 0.0f, 0.0f), vec3(0.0f), vec3(0.0f),
                    radius, invmass, bounce, 2.0f, &grMap.at(e_SPHERE)));
    rigidBodies.push_back(
                    new RigidOBB(vec3(85.0f, 35.0f, -20.0f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f),
                    vec3(radius), invmass, bounce, 0.0f, &grMap.at(e_OBB)));
    rigidBodies.push_back(
                    new RigidOBB(vec3(85.0f, 35.0f, -30.0f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f),
                    vec3(radius), invmass, bounce, 1.0f, &grMap.at(e_OBB)));
    rigidBodies.push_back(
                    new RigidOBB(vec3(85.0f, 35.0f, -40.0f),
                    vec3(0.0f), vec3(0.0f), vec3(0.0f),
                    vec3(radius), invmass, bounce, 2.0f, &grMap.at(e_OBB)));

    { /* Plane setup */
        const float pos{ 50.0f }; // Purely visual, since planes are infinite..
        // Positions of planes
        const vec3 p[3]
        {
            vec3( pos, -10.0f, 0.0f),   // XY
            vec3(-pos*0.875f, -pos, 0.0f),     // Y
            vec3(-pos*2.0f, 0.0f, 0.0f) // X
        };
        // Normals of planes
        const vec3 n[3]
        {
            normalize(vec3(-0.707f, 0.707f, 0.0f)), 
            vec3(0.0f, 1.0f, 0.0f),
            vec3(1.0f, 0.0f, 0.0f)
        };
        float scale{ 9.9f };
        // Scale of planes
        const vec3 s[3]
        {
            vec3(scale*1.125f, scale, scale), 
            vec3(scale, scale, scale),
            vec3(scale, scale, scale)
        };
        // Orientation of planes
        const vec3 o[3]
        {
            vec3( 0.0f, 0.0f, 0.78f), vec3(0.0f, 0.0f, 0.0f),
            vec3(hPI,  hPI, 0.0f),
        };

        planeBuffer.resize(3);
        for (uint i = 0; i < 3; i++)
        {
            planeBuffer[i] = new RigidPlane(p[i], n[i], bounce, frict,
                                            o[i], s[i], &grMap.at(e_PLANE));
        }
    } /* !Plane setup */
}

void LayoutSpinOBBTest(std::vector<RigidBody*> &rigidBodies,
                       std::vector<RigidPlane*> &planeBuffer,
                       std::map<GraphicsSet, GraphicsResouces> &grMap)
{
    // Clean up the previous containers
    for (uint i = 0; i < rigidBodies.size(); i++)
    {
        delete rigidBodies[i];
    }
    rigidBodies.clear();
    for (uint i = 0; i < planeBuffer.size(); i++)
    {
        delete planeBuffer[i];
    }
    planeBuffer.clear();

    const float radius{ 6.0f };
    const float bounce{ 0.1f };
    const float frict{ 0.5f };
    const float invmass{ 0.2f };
    const vec3 spin = vec3(0.0f, 30.0f, 0.0f);
    // Create 3 rigid bodies spining
    rigidBodies.reserve(3);
    rigidBodies.push_back(
                    new RigidOBB(vec3(30.0f, 10.0f, 0.0f),
                    vec3(quarter_pi<float>(), 0.0f, 0.61685027506f),
                    vec3(0.0f), vec3(spin),
                    vec3(radius), invmass, bounce, frict, &grMap.at(e_OBB)));
    rigidBodies.push_back(
                    new RigidOBB(vec3(0.0f, 10.0f, 0.0f),
                    vec3(quarter_pi<float>(), 0.0f, 0.0f),
                    vec3(0.0f), vec3(-spin),
                    vec3(radius), invmass, bounce, frict, &grMap.at(e_OBB)));
    rigidBodies.push_back(
                    new RigidOBB(vec3(-30.0f, 10.0f, 0.0f),
                    vec3(0.0f, 0.0f, 0.0f), vec3(0.0f), vec3(spin),
                    vec3(radius), invmass, bounce, frict, &grMap.at(e_OBB)));

    planeBuffer.resize(1);
    planeBuffer[0] = new RigidPlane(vec3(0.0f), vec3(0.0f, 1.0f, 0.0f),
                    bounce, frict, vec3(0.0f), vec3(10.0f), &grMap.at(e_PLANE));
}

};