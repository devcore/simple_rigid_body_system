/*--------------------------------------------------------\
| File: UserInterface.h                                   |
|x-------------------------------------------------------x|
| Details: The InputHandler creates cameras and processes |
| input to translate and rotate them in the world. It also|
| updates ImGUI with the current mouse position and state.|
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:          Create cameras and updates ImGUI. |
\--------------------------------------------------------*/

/* Note(me): Figure out why it is causing stuttering */

#ifndef USER_INTERFACE_H
#define USER_INTERFACE_H

#include "Util.h"
#include <imgui\imgui.h>

#define IM_ARRAYSIZE(_ARR) ((int)(sizeof(_ARR)/sizeof(*_ARR)))

namespace rigid_body_sys
{
    struct UserInterface
    {
        UserInterface();
        ~UserInterface();
        // Initilises ImGUI
        IMGUI_API void ImGuiInitialise();
        // Destroys ImGUI
        IMGUI_API void ImGuiShutdown();
        // Draw list
        void ImGuiRenderDrawLists(graphics_framework::camera &cam);
    private:
        // Retrieves the default font and bind it as a texture
        IMGUI_API void ImGuiCreateFontsTexture();
        // Creates and binds shaders
        IMGUI_API void ImGuiCreateDeviceObjects();
        // Note: find out if this is necessary
        IMGUI_API void ImGuiInvalidateDeviceObjects();
        // TEMP
        void TempInfoWidget();
        // Graphical and state tracking data
        GLuint     m_FontTexture;
        glm::uint  m_VboHandle,    m_VaoHandle,        m_ElementsHandle;
        glm::int32 m_ShaderHandle, m_VertHandle,       m_FragHandle, 
                   m_AttribLocTex, m_AttribLocProjMtx, m_AttribLocPos,
                   m_AttribLocUV,  m_AttribLocColor;
        // Orthogonal projection matrix
        const glm::mat4 m_orthoProj;
        // Draw command list
        ImDrawData* m_drawData;
    };
}; /* !rigid_body_sys */

#endif /* !USER_INTERFACE_H */