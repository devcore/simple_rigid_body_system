/*--------------------------------------------------------\
| File: RigidPlane.h                                      | 
|x-------------------------------------------------------x|  
| Details: Plane which includes additional attributes for |  
| collision resolutions: friction and restitution coef.   |  
|x-------------------------------------------------------x|  
| Date:                                         June 2015 |  
| Author:                              Andrius Jefremovas |  
| Description:       Rigid plane with required properties.|  
\--------------------------------------------------------*/

#ifndef RIGID_PLANE_H
#define RIGID_PLANE_H

#include "ColliderTypes.h"

namespace rigid_body_sys
{
    struct RigidPlane
    {
        Plane plane;                // Rigid Plane collision shape
        const float RESTITUTION;    // Bouncyness
        const float FRICTION;       // Friction
        // Rendering properties
        glm::vec3 m_orientation;
        glm::vec3 m_scale;
        GraphicsResouces *m_pGraphRes;
        /* Rigid plane:
        - World position of plane
        - Normal (must be normalizsed)
        - Coeficient of restitution aka bouncyness
        - Friction coeficient 
        Appearance:
        - Orientation
        - Scale X-Z
        - GraphicsResources struct */
        RigidPlane(const glm::vec3 &position, const glm::vec3 &normal,
                   const float coefRestitution, const float coefFriction,
                   const glm::vec3 orientation, const glm::vec3 scale,
                   GraphicsResouces *gr) :
                   plane(position, normal),
                   RESTITUTION(coefRestitution), FRICTION(coefFriction),
                   m_orientation(orientation), m_scale(scale),
                   m_pGraphRes(gr) { }
        // Render the plane
        static void Render(const graphics_framework::camera *cam,
                           const std::vector<RigidPlane*> &pbuffer)
        {
            using namespace graphics_framework;
            const glm::mat4 V = cam->get_view();
            const glm::mat4 P = cam->get_projection();
            // Render all planes
            for (glm::uint i = 0; i < pbuffer.size(); i++)
            {
                // Compute model view projection matrix and setup the mesh
                pbuffer[i]->m_pGraphRes->mesh.get_transform().position = pbuffer[i]->plane.position;
                pbuffer[i]->m_pGraphRes->mesh.get_transform().scale = pbuffer[i]->m_scale;
                pbuffer[i]->m_pGraphRes->mesh.get_transform().orientation = glm::quat(pbuffer[i]->m_orientation);
                const glm::mat4 M = pbuffer[i]->m_pGraphRes->mesh.get_transform().get_transform_matrix();
                const glm::mat4 MVP = P * V * M;
                // Bind basic texture shader and send uniforms
                renderer::bind(pbuffer[i]->m_pGraphRes->eff);
                glUniformMatrix4fv(pbuffer[i]->m_pGraphRes->eff.get_uniform_location("MVP"),
                                   1, GL_FALSE, glm::value_ptr(MVP));
                glUniform4fv(pbuffer[i]->m_pGraphRes->eff.get_uniform_location("DBG_COL"), 
                             1, glm::value_ptr(glm::vec4(0.5f, 0.5f, 0.5f, 0.2f)));
                renderer::bind(pbuffer[i]->m_pGraphRes->tex, 0); // Bind blueprint texture
                renderer::render(pbuffer[i]->m_pGraphRes->mesh); // Render point
            }
        }
    };
}; /* !rigid_body_sys */
#endif /* !RIGID_PLANE_H */