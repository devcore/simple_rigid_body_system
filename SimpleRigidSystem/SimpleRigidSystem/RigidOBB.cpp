/*
File: RigidOBB.cpp
--------------------------------------------------------------------------------
For details see RigidOBB.h
*/
#include "RigidOBB.h"
#include "CollisionResponse.h"

using namespace glm;
using namespace graphics_framework;
using namespace rigid_body_sys;

RigidOBB::RigidOBB(const vec3 &pos,         const vec3 &orient, 
                   const vec3 &linVel,      const vec3 &angVel,
                   const vec3 &hExtents,    const float invmass,     
                   const float restitution, const float friction,    
                   GraphicsResouces *gr) :
                   RigidBody(pos, orient, linVel, angVel, invmass,
                             restitution, friction, gr),
                   m_obb(pos, hExtents)
{
    // Compute inverse inertia
    vec3 sqExt2 = hExtents*2.0f;
    sqExt2 *= sqExt2;
    // Value taken from http://www.livephysics.com/physical-constants/mechanics-pc/moment-inertia-uniform-objects/
    const float invX = 1.0f / ((sqExt2.y + sqExt2.z) / (invmass * 12.0f));
    const float invY = 1.0f / ((sqExt2.x + sqExt2.z) / (invmass * 12.0f));
    const float invZ = 1.0f / ((sqExt2.x + sqExt2.y) / (invmass * 12.0f));
    localInvInertia = mat4{ invX, 0.0f, 0.0f, 0.0f,
                            0.0f, invY, 0.0f, 0.0f,
                            0.0f, 0.0f, invZ, 0.0f,
                            0.0f, 0.0f, 0.0f, 1.0f };
    UpdateMatrix();
}

void RigidOBB::Update(const float timeStep)
{
    if (movable)
    {
        UpdateMatrix();
        Integrate(timeStep);
        // Match the OBB volume
        m_obb.position = position;
    }
}

void RigidOBB::Render(const float time, const camera *cam)
{
    // Set mesh to new location and calculate model view projection matrix
    m_pGraphicRes->mesh.get_transform().position = position;
    m_pGraphicRes->mesh.get_transform().orientation = orientation;
    m_pGraphicRes->mesh.get_transform().scale = m_obb.hExtent*2.0f;
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    const mat4 M = m_pGraphicRes->mesh.get_transform().get_transform_matrix();
    const mat4 MVP = P * V * M;
    // Bind shader and send uniforms
    renderer::bind(m_pGraphicRes->eff);
    glUniformMatrix4fv(m_pGraphicRes->eff.get_uniform_location("MVP"),
                       1, GL_FALSE, glm::value_ptr(MVP));
    glUniformMatrix4fv(m_pGraphicRes->eff.get_uniform_location("M"),
                       1, GL_FALSE, value_ptr(M));
    glUniformMatrix3fv(m_pGraphicRes->eff.get_uniform_location("N"),
                      1, GL_FALSE, value_ptr(
                      m_pGraphicRes->mesh.get_transform().get_normal_matrix()));
    glUniform1f(m_pGraphicRes->eff.get_uniform_location("time"), time);
    glUniform3fv(m_pGraphicRes->eff.get_uniform_location("eye_pos"),
                 1, value_ptr(cam->get_position()));
    // Bind material, light and texture
    renderer::bind(m_pGraphicRes->mat, "mat");
    renderer::bind(m_pGraphicRes->light, "light");
    renderer::bind(m_pGraphicRes->tex, 0);
    // Render mesh
    renderer::render(m_pGraphicRes->mesh);
}

void RigidOBB::Integrate(const float timeStep)
{
    // Sanity checks
    assert(invmass > FLOAT_EPSILON);
    assert(length(acceleration) < FLOAT_INF);
    //linVelocity = clamp(linVelocity, -100.0f, 100.0f);
    // Apply gravity
    AddForce(position, vec3(0.0f, m_GRAVITY, 0.0f));
    // Semi-implicit Euler integration
    linVelocity += acceleration * invmass * timeStep;
    position += linVelocity * timeStep;
    angVelocity += vec3(worldInvInertia * vec4(torque, 1.0f));
    // Orientation
    quat spin = (quat(0.0f, angVelocity) * 0.5f) * orientation;
    orientation += spin * timeStep;
    orientation = normalize(orientation);
    // Reset acceleration, torque and apply damping
    acceleration = vec3(0.0f);
    torque = vec3(0.0f);
    linVelocity *= m_DAMPING;
    angVelocity *= 0.99873f;//m_DAMPING;
}

void RigidOBB::UpdateMatrix()
{
    mat4 mR = toMat4(orientation);
    mat4 mT = mat4(1.0f);
    mT = glm::translate(mT, position);
    matWorld = mT * mR;
    worldInvInertia = transpose(mR) * localInvInertia * mR;

    m_obb.uAxis[0] = vec3(mR * vec4(vec3(1, 0, 0), 1.0f));
    m_obb.uAxis[1] = vec3(mR * vec4(vec3(0, 1, 0), 1.0f));
    m_obb.uAxis[2] = vec3(mR * vec4(vec3(0, 0, 1), 1.0f));
}

void RigidOBB::AddForce(const vec3 &point, const vec3 &force)
{
    assert(invmass > FLOAT_EPSILON);
    acceleration += force * (1.0f / invmass);
    torque += cross(point - position, force);
}

bool RigidOBB::TestCollision(RigidPlane &planeBody, CollisionData &cd)
{
    if (OBBPlane(m_obb, planeBody.plane, cd))
    {
        // Multiple collision points
        std::vector<CollisionData> cds;
        vec3 avgPoint = vec3(0.0f);

        for (glm::uint i = 0; i < 8; i++)
        {
            if (PointPlane(GetCorner(i), planeBody.plane, cd))
            {
                avgPoint += cd.point;
                // Note: figure why it slides when contact are on plane at rest
                // Hack: fix sliding (something to do with angular velocity
                if (length(angVelocity) < 0.05f) 
                    cd.point += cd.normal*m_obb.hExtent;
                cds.push_back(cd);
            }
        }
        // Apply all impulses at once
        for (glm::uint i = 0; i < cds.size(); i++)
        {
            ApplyCollisionImpulse(*this, planeBody, cds[i]);
        }
        // For debug just send the avarage point
        if (avgPoint != vec3(0.0f))
        {
            cd.point = avgPoint / static_cast<float>(cds.size());
        }
        return true;
    }
    return false;
}

bool RigidOBB::TestCollision(RigidBody &body, CollisionData &cd)
{
    return (body.TestCollision(*this, m_obb, cd));
}

bool RigidOBB::TestCollision(RigidBody &body, Sphere &sphere, CollisionData &cd)
{
    if (SphereOBB(sphere, m_obb, cd))
    {
        ApplyCollisionImpulse(*this, body, cd);
        return true;
    }
    return false;
}

bool RigidOBB::TestCollision(RigidBody &body, AABB &aabb, CollisionData &cd)
{
    if (OBBAABB(m_obb, aabb, cd))
    {
        ApplyCollisionImpulse(*this, *static_cast<RigidAABB*>(&body), cd);
        return true;
    }
    return false;
}

bool RigidOBB::TestCollision(RigidBody &body, OBB &obb, CollisionData &cd)
{
    if (OBBOBB(m_obb, obb, cd))
    {
        ApplyCollisionImpulse(body, *this, cd);
        return true;
    }
    return false;
}

vec3 RigidOBB::GetCorner(const uint index)
{
    assert("OBB only has 8 corners" && 0 <= index && index <= 7);
    switch (index)
    {
        default: // Note:
        case 0: return vec3(matWorld * vec4(-m_obb.hExtent.x, -m_obb.hExtent.y, -m_obb.hExtent.z, 1.0f));
        case 1: return vec3(matWorld * vec4(-m_obb.hExtent.x, -m_obb.hExtent.y, +m_obb.hExtent.z, 1.0f));
        case 2: return vec3(matWorld * vec4(-m_obb.hExtent.x,  m_obb.hExtent.y, -m_obb.hExtent.z, 1.0f));
        case 3: return vec3(matWorld * vec4(-m_obb.hExtent.x,  m_obb.hExtent.y, +m_obb.hExtent.z, 1.0f));
        case 4: return vec3(matWorld * vec4( m_obb.hExtent.x, -m_obb.hExtent.y, -m_obb.hExtent.z, 1.0f));
        case 5: return vec3(matWorld * vec4( m_obb.hExtent.x, -m_obb.hExtent.y, +m_obb.hExtent.z, 1.0f));
        case 6: return vec3(matWorld * vec4( m_obb.hExtent.x,  m_obb.hExtent.y, -m_obb.hExtent.z, 1.0f));
        case 7: return vec3(matWorld * vec4( m_obb.hExtent.x,  m_obb.hExtent.y, +m_obb.hExtent.z, 1.0f));
    }
}

Segment RigidOBB::GetEdge(const uint index)
{
    assert("OBB only has 12 edges" && 0 <= index && index <= 11);
    switch (index)
    {
        default: // Note:
        case 0:  return Segment(GetCorner(0), GetCorner(1));
        case 1:  return Segment(GetCorner(0), GetCorner(2));
        case 2:  return Segment(GetCorner(0), GetCorner(4));
        case 3:  return Segment(GetCorner(1), GetCorner(3));
        case 4:  return Segment(GetCorner(1), GetCorner(5));
        case 5:  return Segment(GetCorner(2), GetCorner(3));
        case 6:  return Segment(GetCorner(2), GetCorner(6));
        case 7:  return Segment(GetCorner(3), GetCorner(7));
        case 8:  return Segment(GetCorner(4), GetCorner(5));
        case 9:  return Segment(GetCorner(4), GetCorner(6));
        case 10: return Segment(GetCorner(5), GetCorner(7));
        case 11: return Segment(GetCorner(6), GetCorner(7));
    }
}

Plane RigidOBB::GetFace(const uint index)
{
    assert("OBB only has 6 faces" && 0 <= index && index <= 11);
    switch (index)
    {
        default: 
        case 0:  return Plane(GetFaceCenterPt(0), -m_obb.uAxis[0]);
        case 1:  return Plane(GetFaceCenterPt(1),  m_obb.uAxis[0]);
        case 2:  return Plane(GetFaceCenterPt(2), -m_obb.uAxis[1]);
        case 3:  return Plane(GetFaceCenterPt(3),  m_obb.uAxis[1]);
        case 4:  return Plane(GetFaceCenterPt(4), -m_obb.uAxis[2]);
        case 5:  return Plane(GetFaceCenterPt(5),  m_obb.uAxis[2]);
    }
}

vec3 RigidOBB::GetFaceCenterPt(const uint index)
{
    assert("OBB only has 6 faces" && 0 <= index && index <= 11);
    switch (index)
    {
        default: 
        case 0:  return vec3(m_obb.position - m_obb.hExtent.x * m_obb.uAxis[0]);
        case 1:  return vec3(m_obb.position + m_obb.hExtent.x * m_obb.uAxis[0]);
        case 2:  return vec3(m_obb.position - m_obb.hExtent.y * m_obb.uAxis[1]);
        case 3:  return vec3(m_obb.position + m_obb.hExtent.y * m_obb.uAxis[1]);
        case 4:  return vec3(m_obb.position - m_obb.hExtent.x * m_obb.uAxis[2]);
        case 5:  return vec3(m_obb.position + m_obb.hExtent.x * m_obb.uAxis[2]);
    }
}