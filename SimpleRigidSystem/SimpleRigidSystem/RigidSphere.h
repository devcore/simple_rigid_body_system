/*--------------------------------------------------------\
| File: RigidSphere.h                                     |
|x-------------------------------------------------------x|
| Details: Rigid sphere is a derived class that uses a    |
| sphere collider for collision detection and resolution. |
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:                      Spherical rigid body. |
\--------------------------------------------------------*/

#ifndef RIGID_SPHERE_H
#define RIGID_SPHERE_H

#include "RigidBody.h"

namespace rigid_body_sys
{
    struct RigidSphere : public RigidBody
    {
        Sphere m_sphere;  // Rigid Sphere collision shape
        /* RigidSphere: position, orientation, linear and angular velocity,
                        radius, inverse mass, coeficients of restitution and 
                        friction, graphcis resources.*/
        RigidSphere(const glm::vec3 &pos,    const glm::vec3 &orient, 
                    const glm::vec3 &linVel, const glm::vec3 &angVel,
                    const float radius,      const float invmass,
                    const float restitution, const float friction,    
                    GraphicsResouces *gr);
        // Semi-implicit Euler integration
        void Integrate(const float timeStep);
        // Calls integrate every physics update and centers the bounding type
        void Update(const float timeStep);
        // Renders a sphere
        void Render(const float time,
                    const graphics_framework::camera *cam);
        // Update world matrix and world inertia matrix
        void UpdateMatrix();
        // Computes acceleration, torque given a force and point then applies it
        void AddForce(const glm::vec3 &point, const glm::vec3 &force);
        // Test if sphere is colliding another object
        bool TestCollision(RigidBody &body, CollisionData &cd);
        bool TestCollision(RigidBody &body, OBB &obb, CollisionData &cd);
        bool TestCollision(RigidBody &body, AABB &aabb, CollisionData &cd);
        bool TestCollision(RigidBody &body, Sphere &sphere, CollisionData &cd);
        // Separate case for rigidplane
        bool TestCollision(RigidPlane &plane, CollisionData &cd);
    };
}; /* !rigid_body_sys */

#endif /* !RIGID_SPHERE_H */