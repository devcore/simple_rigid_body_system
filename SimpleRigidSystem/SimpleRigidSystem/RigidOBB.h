/*--------------------------------------------------------\
| File: RigidOBB.h                                        | 
|x-------------------------------------------------------x| 
| Details: Rigid OBB is a derived class that uses a orien-| 
| bounding box collider to handle collisions.             | 
|x-------------------------------------------------------x| 
| Date:                                         June 2015 | 
| Author:                              Andrius Jefremovas | 
| Description:          Oriented bounding box rigid body. | 
\--------------------------------------------------------*/

#ifndef RIGID_OBB_H
#define RIGID_OBB_H

#include "RigidBody.h"

namespace rigid_body_sys
{
    struct RigidOBB : public RigidBody
    {
        OBB m_obb;  // Oriented bounding box collision shape
        /* RigidOBB: position, orientation, linear and angular velocity,
                     half extents, inverse mass, coeficients of restitution 
                     and friction, graphcis resources.*/
        RigidOBB(const glm::vec3 &pos,      const glm::vec3 &orient, 
                 const glm::vec3 &linVel,   const glm::vec3 &angVel,
                 const glm::vec3 &hExtents, const float invmass,     
                 const float restitution,   const float friction,    
                 GraphicsResouces *gr);
        // Semi-implicit Euler integration
        void Integrate(const float timeStep);
        // Calls integrate every physics update and centers the bounding shape
        void Update(const float timeStep);
        // Renders a oriented bounding box
        void Render(const float time,
                    const graphics_framework::camera *cam);
        // Update world matrix and world inertia matrix
        void UpdateMatrix();
        // Computes acceleration, torque given a force and point then applies it
        void AddForce(const glm::vec3 &point, const glm::vec3 &force);
        // Test if OBB is colliding another object
        bool TestCollision(RigidBody &body, CollisionData &cd);
        bool TestCollision(RigidBody &body, OBB &obb, CollisionData &cd);
        bool TestCollision(RigidBody &body, AABB &aabb, CollisionData &cd);
        bool TestCollision(RigidBody &body, Sphere &sphere, CollisionData &cd);
        // Separate case for rigidplane
        bool TestCollision(RigidPlane &plane, CollisionData &cd);
        // Compute obbs corner in world coordinates
        glm::vec3 GetCorner(const glm::uint index);
        // Compute obbs edge in world coordinates
        Segment GetEdge(const glm::uint index);
        // Compute obbs center point on a face in world coordinates
        glm::vec3 GetFaceCenterPt(const glm::uint index);
        // Return the face as a plane
        Plane GetFace(const glm::uint index);
    };
}; /* !rigid_body_sys */

#endif /* !RIGID_OBB_H */