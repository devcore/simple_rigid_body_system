/*--------------------------------------------------------\
| File: Layouts.h                                         |
|x-------------------------------------------------------x|  
| Details: Places rigid bodies in various test case confi-|  
| guration to test limitation of the simulation.          |   
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:       Test cases for rigid body simulation.|
\--------------------------------------------------------*/
#ifndef CORE_H
#define CORE_H

#include "Util.h"
#include "RigidBody.h"
#include "RigidAABB.h"
#include "RigidOBB.h"
#include "RigidSphere.h"
#include "RigidPlane.h"

namespace rigid_body_sys
{
    // Create all shapes on a single plane
    void LayoutDefault(std::vector<RigidBody*> &rigidBodies,
                       std::vector<RigidPlane*> &planeBuffer,
                       std::map<GraphicsSet, GraphicsResouces> &grMap);

    // Create all shapes contacting eachother
    void LayoutContactTest(std::vector<RigidBody*> &rigidBodies,
                           std::vector<RigidPlane*> &planeBuffer,
                           std::map<GraphicsSet, GraphicsResouces> &grMap);

    // Creates a sphere of rigid spheres
    void LayoutSphereSmashTest(std::vector<RigidBody*> &rigidBodies,
                               std::vector<RigidPlane*> &planeBuffer,
                               std::map<GraphicsSet, GraphicsResouces> &grMap);

    // Creates a wall of rigid bodies and smashes a sphere at them
    void LayoutMixWallTest(std::vector<RigidBody*> &rigidBodies,
                           std::vector<RigidPlane*> &planeBuffer,
                           std::map<GraphicsSet, GraphicsResouces> &grMap);

    // Tests rigid body friction using a plane as a slop
    void LayoutFrictionTest(std::vector<RigidBody*> &rigidBodies,
                            std::vector<RigidPlane*> &planeBuffer,
                            std::map<GraphicsSet, GraphicsResouces> &grMap);

    // Creates a OOB that spins on it's edge
    void LayoutSpinOBBTest(std::vector<RigidBody*> &rigidBodies,
                           std::vector<RigidPlane*> &planeBuffer,
                           std::map<GraphicsSet, GraphicsResouces> &grMap);

}; /* !rigid_body_sys */

#endif /* !Layouts */