/*--------------------------------------------------------\
| File: main.cpp                                          |  
|x-------------------------------------------------------x|  
| Details: An inplementation of a simple rigid body simu- |  
| lation. Uses basic overlap tests to detect collisitons. |
| When a collision is detected an impulse force is applied|
| to both rigid bodies that consists of linear, angular   |
| friction and correction forces. The linear and angular  |
| forces separate the bodies while the correct force is   |
| used to prevent sinking while stacking bodies.          |
|x-------------------------------------------------------x|
| CONTROLS:                                               |
| SPACEBAR - Simulation Start/Pause                       |
| TAB      - Changes camera type                          |
| Alt      - Unlock/Locks mouse                           |
| ESC      - Closes application                           |
| 1-4      - Test case layouts                            |
|x-------------------------------------------------------x|
| FREE Camera                                             |
| WASD QE      - Movement                                 |
| Mouse/Arrows - Rotation                                 |
| Shift        - HOLD to increase movement speed          |
|x-------------------------------------------------------x|
| ARC-BALL Camera:                                        |     _____
| WASD/Mouse - Rotation                                   |     \   /
| QE/Scroll  - Zoom/Distance                              |     /   \
| Shift      - HOLD to increase speed of zooming          |    /  A  \
|x-------------------------------------------------------x|   /  / \  \  
| Date:                                         June 2015 |  /  /   \  \  
| Author:                              Andrius Jefremovas | /  /_____\  \  
| Description: Entry point - creates system and links it. | \___________/  
\--------------------------------------------------------*/
#include "Core.h"

void main()
{
    // Initialise rigid body system: time step | tick rate
    rigid_body_sys::Core core{ 0.01f, 120 };
    // Initialise rendering framework
    graphics_framework::app application;
    // Link the framework to the simulation
    application.set_initialise([&core](){ return(core.Initialise()); });
    application.set_update([&core](const float dt){ return(core.Update(dt)); });
    application.set_render([&core](const float t){ return(core.Render(t)); });
    application.run();
}