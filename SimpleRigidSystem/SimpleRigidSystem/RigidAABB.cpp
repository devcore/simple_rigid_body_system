/*
File: RigidAABB.cpp
--------------------------------------------------------------------------------
For details see RigidAABB.h
*/
#include "RigidAABB.h"
#include "CollisionResponse.h"

using namespace glm;
using namespace graphics_framework;
using namespace rigid_body_sys;

RigidAABB::RigidAABB(const glm::vec3 &pos,      const glm::vec3 &orient, 
                     const glm::vec3 &linVel,   const glm::vec3 &angVel,
                     const glm::vec3 &hExtents, const float invmass,     
                     const float restitution,   const float friction,    
                     GraphicsResouces *gr) :
                     RigidBody(pos, orient, linVel, angVel, invmass,
                               restitution, friction, gr),
                     m_aabb(pos, hExtents)
{
    // Inverse inertia
    vec3 sqExt2 = hExtents*2.0f;
    sqExt2 *= sqExt2;
    // Value taken from http://www.livephysics.com/physical-constants/mechanics-pc/moment-inertia-uniform-objects/
    float invX = 1.0f / ((sqExt2.y + sqExt2.z) / (invmass * 12.0f));
    float invY = 1.0f / ((sqExt2.x + sqExt2.z) / (invmass * 12.0f));
    float invZ = 1.0f / ((sqExt2.x + sqExt2.y) / (invmass * 12.0f));
    localInvInertia = mat4{ invX, 0.0f, 0.0f, 0.0f,
                            0.0f, invY, 0.0f, 0.0f,
                            0.0f, 0.0f, invZ, 0.0f,
                            0.0f, 0.0f, 0.0f, 1.0f };
}

void RigidAABB::Update(const float timestep)
{
    if (movable)
    {
        // Match the sphere volume
        Integrate(timestep);
        m_aabb.position = position;
    }
}

void RigidAABB::Render(const float time, const camera *cam)
{
    // Set mesh to new location and calculate model view projection matrix
    m_pGraphicRes->mesh.get_transform().position = position;
    m_pGraphicRes->mesh.get_transform().orientation = orientation;
    m_pGraphicRes->mesh.get_transform().scale = m_aabb.hExtent*2.0f;
    const mat4 V = cam->get_view();
    const mat4 P = cam->get_projection();
    const mat4 M = m_pGraphicRes->mesh.get_transform().get_transform_matrix();
    const mat4 MVP = P * V * M;
    // Bind shader and send uniforms
    renderer::bind(m_pGraphicRes->eff);
    glUniformMatrix4fv(m_pGraphicRes->eff.get_uniform_location("MVP"),
                       1, GL_FALSE, glm::value_ptr(MVP));
    glUniformMatrix4fv(m_pGraphicRes->eff.get_uniform_location("M"),
                       1, GL_FALSE, value_ptr(M));
    glUniformMatrix3fv(m_pGraphicRes->eff.get_uniform_location("N"),
                      1, GL_FALSE, value_ptr(
                      m_pGraphicRes->mesh.get_transform().get_normal_matrix()));
    glUniform1f(m_pGraphicRes->eff.get_uniform_location("time"), time);
    glUniform3fv(m_pGraphicRes->eff.get_uniform_location("eye_pos"),
                 1, value_ptr(cam->get_position()));
    // Bind material, light and texture
    renderer::bind(m_pGraphicRes->mat, "mat");
    renderer::bind(m_pGraphicRes->light, "light");
    renderer::bind(m_pGraphicRes->tex, 0);
    // Render mesh
    renderer::render(m_pGraphicRes->mesh);
}

void RigidAABB::Integrate(const float timeStep)
{
    // Sanity checks
    assert(invmass > FLOAT_EPSILON);
    assert(length(acceleration) < FLOAT_INF);
    // Apply gravity
    AddForce(vec3(0.0f, m_GRAVITY, 0.0f));
    // Semi-implicit Euler integration
    linVelocity += acceleration * invmass * timeStep;
    position += linVelocity * timeStep;
    // Apply dampening and reset acceleration
    linVelocity *= m_DAMPING;
    acceleration = vec3(0.0f);
}

void RigidAABB::AddForce(const vec3 &force)
{
    assert(invmass > FLOAT_EPSILON);
    acceleration += force * (1.0f / invmass);
}

bool RigidAABB::TestCollision(RigidPlane &planeBody, CollisionData &cd)
{
    if (AABBPlane(m_aabb, planeBody.plane, cd))
    {
        ApplyCollisionImpulse(*this, planeBody, cd);
        return true;
    }
    return false;
}

bool RigidAABB::TestCollision(RigidBody &body, CollisionData &cd) 
{
    return (body.TestCollision(*this, m_aabb, cd));
}

bool RigidAABB::TestCollision(RigidBody &body, Sphere &sphere, CollisionData &cd)
{
    if (SphereAABB(sphere, m_aabb, cd))
    {
        ApplyCollisionImpulse(body, *this, cd);
        return true;
    }
    return false;
}

bool RigidAABB::TestCollision(RigidBody &body, AABB &aabb, CollisionData &cd)
{
    if (AABBAABB(m_aabb, aabb, cd))
    {
        ApplyCollisionImpulse(*this, *static_cast<RigidAABB*>(&body), cd);
        return true;
    }
    return false;
}

bool RigidAABB::TestCollision(RigidBody &body, OBB &obb, CollisionData &cd)
{
    if (OBBAABB(obb, m_aabb, cd))
    {
        ApplyCollisionImpulse(body, *this, cd);
        return true;
    }
    return false;
}