/*--------------------------------------------------------\
| File: CollisionDetection.h                              |
|x-------------------------------------------------------x|
| Details: Impulse based collision response methods use   |
| manifold defined in CollisionDetection.h to compute to  |
| linear and angular forces required to separate two      |
| intersecting rigid bodies.                              |
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description:     Collision response between two objects.|
\--------------------------------------------------------*/
#ifndef COLLISION_RESPONSE_H
#define COLLISION_RESPONSE_H

#include "CollisionDetection.h"
#include "RigidBody.h"
#include "RigidPlane.h"
#include "RigidAABB.h"

namespace rigid_body_sys
{
    // Generic collision response for intersecting rigid bodies
    void ApplyCollisionImpulse(RigidBody &bodyA, RigidBody &bodyB,
                               const CollisionData &cd);
    // Specialised response for anything vs AABB to exclude angular force
    void ApplyCollisionImpulse(RigidBody &bodyA, RigidAABB &bodyB,
                               const CollisionData &cd);
    // Specialised collision response for AABBs to exclude angular force
    void ApplyCollisionImpulse(RigidAABB &bodyA, RigidAABB &bodyB,
                               const CollisionData &cd);
    /* Specialised collision response for Planes which assumes they have 
       infinite mass and are not able move */
    void ApplyCollisionImpulse(RigidBody &bodyA, RigidPlane &bodyB,
                               const CollisionData &cd);
    /* Specialised collision response for Planes and AABB to exclude
       AABB angular velocity and to assume planes have infinite mass*/
    void ApplyCollisionImpulse(RigidAABB &bodyA, RigidPlane &bodyB,
                               const CollisionData &cd);
}; /* !rigid_body_sys */

#endif /* !COLLISION_DETECTION_H */