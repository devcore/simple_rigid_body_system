/*
File: CollisionResponse.cpp
--------------------------------------------------------------------------------
For details see CollisionResponse.h
*/
#include "CollisionResponse.h"

#define CORRECT 1.0f/64.0f
#define SLOP 0.02f

using namespace glm;
namespace rigid_body_sys
{

inline float ComputeFriction(const float avgFriction, const float jNormal, 
                             const float jTangent)
{
    // Clamp tangental component - Coulomb's Law
    if (abs(jTangent) < abs(jNormal * avgFriction))
    {
        return jTangent;
    }
    else
    {
        return (-jNormal * avgFriction); // dynamic friction.
    }

}

// Generic collision response for intersecting rigid bodies
void ApplyCollisionImpulse(RigidBody &bodyA, RigidBody &bodyB, 
                           const CollisionData &cd)
{
    // Check if both are object movable or massless
    if ((bodyA.invmass + bodyB.invmass) == 0.0 ||
       (!bodyA.movable && !bodyB.movable))
    {
        return; // No collision response needed
    }
    const mat4 worldInertiaA = bodyA.worldInvInertia;
    const mat4 worldInertiaB = bodyB.worldInvInertia;
    const vec3 rA = cd.point - bodyA.position;
    const vec3 rB = cd.point - bodyB.position;
    const vec3 velA = bodyA.linVelocity + cross(bodyA.angVelocity, rA);
    const vec3 velB = bodyB.linVelocity + cross(bodyB.angVelocity, rB);
    const vec3 dv = velA - velB;    // Relative Velocity
    const float massSum = bodyA.invmass + bodyB.invmass;
    // If the bodies are moving away no impulse is required
    float relativeMovement = -dot(dv, cd.normal);
    if (relativeMovement < -0.01f)
    {
        return;
    }
    // Normal Impulse
    float normDiv = massSum + dot(cd.normal,
        cross(vec3(worldInertiaA * vec4(cross(rA, cd.normal), 1.0f)), rA) +
        cross(vec3(worldInertiaB * vec4(cross(rB, cd.normal), 1.0f)), rB));
    // Coeficient of restitution take the average
    float e = (bodyA.RESTITUTION + bodyB.RESTITUTION) * 0.5f;
    // Magnitude of normal collision impulse
    float jn = -(1.0f + e) * dot(dv, cd.normal) / normDiv;
    // Hack to stop sinking bias impulse proportianal to penetration
    jn += (cd.penetration * 1.5f);

    // Tangent impulse
    vec3 tangent = dv - (dot(dv, cd.normal) * cd.normal);
    // Cannot normalize a zero length vector
    tangent = (length2(tangent) > 0.0f) ? normalize(tangent) : vec3(0.0f);
    float tangDiv = massSum + dot(tangent,
        cross(vec3(worldInertiaA * vec4(cross(rA, tangent), 1.0f)), rA) +
        cross(vec3(worldInertiaB * vec4(cross(rB, tangent), 1.0f)), rB));
    // Magnitude of tangent impulse
    float jt = -dot(dv, tangent) / tangDiv;
    // Clamp to apply friction
    jt = ComputeFriction((bodyA.FRICTION + bodyB.FRICTION) * 0.5f, jn, jt);

    // Correction impulse
    float jc = max(cd.penetration - SLOP, 0.0f) / massSum * CORRECT;

    if (bodyA.movable)
    {
        // Apply normal impulse
        bodyA.linVelocity += cd.normal * jn * bodyA.invmass;
        bodyA.angVelocity += vec3(worldInertiaA * 
                             vec4(cross(rA, cd.normal * jn), 1.0f));
        // Apply tangental impulse
        bodyA.linVelocity += tangent * jt * bodyA.invmass;
        bodyA.angVelocity += vec3(worldInertiaB * 
                             vec4(cross(rA, tangent * jt), 1.0f));
        // Apply correction impulse
        bodyA.position += cd.normal * jc * bodyA.invmass;
    }
    if (bodyB.movable)
    {
        // Apply normal impulse
        bodyB.linVelocity -= cd.normal * jn * bodyB.invmass;
        bodyB.angVelocity -= vec3(worldInertiaA * 
                             vec4(cross(rB, cd.normal * jn), 1.0f));
        // Apply tangental impulse
        bodyB.linVelocity -= tangent * jt * bodyB.invmass;
        bodyB.angVelocity -= vec3(worldInertiaB * 
                             vec4(cross(rB, tangent * jt), 1.0f));
        // Apply correction impulse
        bodyB.position += cd.normal * jc * bodyB.invmass;
    }
}

// Specialised collision response for anything vs AABB to exclude angular force
void ApplyCollisionImpulse(RigidBody &bodyA, RigidAABB &bodyB, 
                           const CollisionData &cd)
{
    // Check if both are object movable or massless
    if ((bodyA.invmass + bodyB.invmass) == 0.0 ||
        (!bodyA.movable && !bodyB.movable))
    {
        return; // No collision response needed
    }
    const mat4 worldInertiaA = bodyA.worldInvInertia;
    const mat4 worldInertiaB = bodyB.worldInvInertia;
    const vec3 rA = cd.point - bodyA.position;
    const vec3 rB = cd.point - bodyB.position;
    const vec3 vA = bodyA.linVelocity + cross(bodyA.angVelocity, rA);
    const vec3 vB = bodyB.linVelocity + cross(bodyB.angVelocity, rB);
    const vec3 dv = vA - vB;    // Relative Velocity
    const float massSum = bodyA.invmass + bodyB.invmass;
    // If the bodies are moving away no impulse is required
    float relativeMovement = -dot(dv, cd.normal);
    if (relativeMovement < -0.01f)
    {
        return;
    }
    // Normal impulse
    float normdiv = massSum + dot(cd.normal,
        cross(vec3(worldInertiaA * vec4(cross(rA, cd.normal), 1.0f)), rA) +
        cross(vec3(worldInertiaB * vec4(cross(rB, cd.normal), 1.0f)), rB));
    // Coeficient of restitution take the average
    float e = (bodyA.RESTITUTION + bodyB.RESTITUTION) * 0.5f;
    // Magnitude of normal collision impulse
    float jn = -(1.0f + e) * dot(dv, cd.normal) / normdiv;
    // Hack to stop sinking bias impulse proportianal to penetration
    jn += (cd.penetration * 1.5f);

    // Tangent impulse
    vec3 tangent = dv - (dot(dv, cd.normal) * cd.normal);
    // Cannot normalize a zero length vector
    tangent = (length2(tangent) > 0.0f) ? normalize(tangent) : vec3(0.0f);
    float tangDiv = massSum + dot(tangent,
        cross(vec3(worldInertiaA * vec4(cross(rA, tangent), 1.0f)), rA) +
        cross(vec3(worldInertiaB * vec4(cross(rB, tangent), 1.0f)), rB));
    float jt = -dot(dv, tangent) / tangDiv;
    // Clamp to apply friction
    jt = ComputeFriction((bodyA.FRICTION + bodyB.FRICTION) * 0.5f, jn, jt);

    // Correction impulse
    float jc = max(cd.penetration - SLOP, 0.0f) / massSum * CORRECT;

    // Apply impulses to movable bodies
    if (bodyA.movable)
    {
        // Apply normal impulse
        bodyA.linVelocity += cd.normal * jn * bodyA.invmass;
        bodyA.angVelocity += vec3(worldInertiaA * 
                             vec4(cross(rA, cd.normal * jn), 1.0f));
        // Apply tangental impulse
        bodyA.linVelocity += tangent * jt * bodyA.invmass;
        bodyA.angVelocity += vec3(worldInertiaA * 
                             vec4(cross(rA, tangent * jt), 1.0f));
        // Apply correction impulse
        bodyA.position += cd.normal * jc * bodyA.invmass;
    }
    if (bodyB.movable)
    {
        // Apply normal impulse
        bodyB.linVelocity -= cd.normal * jn * bodyB.invmass;
        // Apply tangental impulse
        bodyB.linVelocity -= tangent * jt * bodyB.invmass;
        // Apply correction impulse
        bodyB.position -= cd.normal * jc * bodyB.invmass;
    }
}

// Specialised collision response for AABBs to exclude angular force
void ApplyCollisionImpulse(RigidAABB &bodyA, RigidAABB &bodyB, 
                           const CollisionData &cd)
{
    // Check if both are object movable or massless
    if ((bodyA.invmass + bodyB.invmass) == 0.0 ||
        (!bodyA.movable && !bodyB.movable))
    {
        return; // No collision response needed
    }
    const mat4 worldInertiaA = bodyA.worldInvInertia;
    const mat4 worldInertiaB = bodyB.worldInvInertia;
    const vec3 rA = cd.point - bodyA.position;
    const vec3 rB = cd.point - bodyB.position;
    const vec3 vA = bodyA.linVelocity + cross(bodyA.angVelocity, rA);
    const vec3 vB = bodyB.linVelocity + cross(bodyB.angVelocity, rB);
    const vec3 dv = vA - vB;    // Relative Velocity
    const float massSum = bodyA.invmass + bodyB.invmass;
    // If the bodies are moving away no impulse is required
    float relativeMovement = -dot(dv, cd.normal);
    if (relativeMovement < -0.01f)
    {
        return;
    }
    // Normal impulse
    float normdiv = massSum + dot(cd.normal,
        cross(vec3(worldInertiaA * vec4(cross(rA, cd.normal), 1.0f)), rA) +
        cross(vec3(worldInertiaB * vec4(cross(rB, cd.normal), 1.0f)), rB));
    // Coeficient of restitution take the average
    float e = (bodyA.RESTITUTION + bodyB.RESTITUTION) * 0.5f;
    // Magnitude of normal collision impulse
    float jn = -(1.0f + e) * dot(dv, cd.normal) / normdiv;
    // Hack to stop sinking bias impulse proportianal to penetration
    jn += (cd.penetration * 1.5f);

    // Tangent impulse
    vec3 tangent = dv - (dot(dv, cd.normal) * cd.normal);
    // Cannot normalize a zero length vector
    tangent = (length2(tangent) > 0.0f) ? normalize(tangent) : vec3(0.0f);
    float tangDiv = massSum + dot(tangent,
        cross(vec3(worldInertiaA * vec4(cross(rA, tangent), 1.0f)), rA) +
        cross(vec3(worldInertiaB * vec4(cross(rB, tangent), 1.0f)), rB));
    float jt = -dot(dv, tangent) / tangDiv;
    // Clamp to apply friction
    jt = ComputeFriction((bodyA.FRICTION + bodyB.FRICTION) * 0.5f, jn, jt);

    // Correction impulse
    float jc = max(cd.penetration - SLOP, 0.0f) / massSum * CORRECT;

    if (bodyA.movable)
    {
        // Apply normal impulse 
        bodyA.linVelocity += cd.normal * jn * bodyA.invmass;
        // Apply tangental impulse
        bodyA.linVelocity += tangent * jt * bodyA.invmass;
        // Apply correction impulse
        bodyA.position += cd.normal * jc * bodyA.invmass;
    }
    if (bodyB.movable)
    {
        // Apply normal impulse
        bodyB.linVelocity -= cd.normal * jn * bodyB.invmass;
        // Apply tangental impulse
        bodyB.linVelocity -= tangent * jt * bodyB.invmass;
        // Apply correction impulse
        bodyB.position -= cd.normal * jc * bodyB.invmass;
    }
}

/* Specialised collision response for Planes which assumes they have infinite
   mass and are not able move */
void ApplyCollisionImpulse(RigidBody &bodyA, RigidPlane &bodyB, 
                           const CollisionData &cd)
{
    // Check if both are object movable
    if (bodyA.invmass == 0.0 || !bodyA.movable)
    {
        return; // No collision response needed
    }
    const vec3 rA = cd.point - bodyA.position;
    const vec3 vA = bodyA.linVelocity + cross(bodyA.angVelocity, rA);
    const mat4 worldInertiaA = bodyA.worldInvInertia;
    // If an object is moving away from the plane no impulse is needed
    float relativeMovement = -dot(vA, cd.normal);
    if (relativeMovement < -0.01f)
    {
        return;
    }
    // Normal Impulse
    float normdiv = bodyA.invmass + dot(cd.normal,
        cross(vec3(vec4(cross(rA, cd.normal), 1.0f) * worldInertiaA), rA));
    // Coeficient of restitution take the average
    float e = (bodyA.RESTITUTION + bodyB.RESTITUTION) * 0.5f;
    // Magnitude of normal collision impulse
    float jn = -(1.0f + e) * dot(vA, cd.normal) / normdiv;
    // Hack to stop sinking bias impulse proportianal to penetration
    jn += (cd.penetration * 1.5f);

    // Tangent Impulse
    vec3 tangent = vA - (dot(vA, cd.normal) * cd.normal);
    // Cannot normalize a zero length vector
    tangent = (length2(tangent) > 0.0f) ? normalize(tangent) : vec3(0.0f);
    float tangDiv = bodyA.invmass + dot(tangent,
        cross(vec3(vec4(cross(rA, tangent), 1.0f)* worldInertiaA), rA));
    // Magnitude of tangent impulse
    float jt = -dot(vA, tangent) / tangDiv;
    // Clamp to apply friction
    jt = ComputeFriction((bodyA.FRICTION + bodyB.FRICTION) * 0.5f, jn, jt);

    // Correction impulse magnitude
    float jc = max(cd.penetration - SLOP, 0.0f) / bodyA.invmass * CORRECT;

    // Apply normal impulse
    bodyA.linVelocity += cd.normal * jn * bodyA.invmass;
    bodyA.angVelocity += vec3(vec4(cross(rA, cd.normal * jn), 1.0f) * worldInertiaA);
    // Apply tangental impulse
    bodyA.linVelocity += tangent * jt * bodyA.invmass;
    bodyA.angVelocity += vec3(vec4(cross(rA, tangent * jt), 1.0f) * worldInertiaA);
    // Apply correction impulse
    bodyA.position += cd.normal * jc * bodyA.invmass;
}

/* Specialised collision response for Planes and AABB to exclude
   AABB angular velocity and to assume planes have infinite mass*/
void ApplyCollisionImpulse(RigidAABB &bodyA, RigidPlane &bodyB, 
                           const CollisionData &cd)
{
    // Check if AABB is movable or has no mass
    if (bodyA.invmass == 0.0 || !bodyA.movable)
    {
        return; // No collision response needed
    }
    const vec3 rA = cd.point - bodyA.position;
    const vec3 dv = bodyA.linVelocity + cross(bodyA.angVelocity, rA);
    const mat4 worldInertiaA = bodyA.worldInvInertia;
    // If an object is moving away from the plane no impulse is needed
    float relativeMovement = -dot(dv, cd.normal);
    if (relativeMovement < -0.01f)
    {
        return;
    }
    // Normal impulse
    float normdiv = bodyA.invmass + dot(cd.normal,
        cross(vec3(worldInertiaA * vec4(cross(rA, cd.normal), 1.0f)), rA));
    // Coeficient of restitution take the average
    float e = (bodyA.RESTITUTION + bodyB.RESTITUTION) * 0.5f;
    // Magnitude of normal collision impulse
    float jn = -(1.0f + e) * dot(dv, cd.normal) / normdiv;
    // Hack to stop sinking bias impulse proportianal to penetration
    jn += (cd.penetration * 1.5f);

    // Tangent impulse
    vec3 tangent = dv - (dot(dv, cd.normal) * cd.normal);
    // Cannot normalize a zero length vector
    tangent = (length2(tangent) > 0.0f) ? normalize(tangent) : vec3(0.0f);
    float tangDiv = bodyA.invmass + dot(tangent,
        cross(vec3(worldInertiaA * vec4(cross(rA, tangent), 1.0f)), rA));
    // Magnitude of tangent impulse
    float jt = -dot(dv, tangent) / tangDiv;
    // Clamp to apply friction
    jt = ComputeFriction((bodyA.FRICTION + bodyB.FRICTION) * 0.5f, jn, jt);

    // Correction impulse
    float jc = max(cd.penetration - SLOP, 0.0f) / bodyA.invmass * CORRECT;

    // Apply normal impulse
    bodyA.linVelocity += cd.normal * jn * bodyA.invmass;
    // Apply tangental impulse
    bodyA.linVelocity += tangent * jt * bodyA.invmass;
    // Apply correction impulse
    bodyA.position += cd.normal * jc * bodyA.invmass;
}

}; /* !rigid_body_sys */