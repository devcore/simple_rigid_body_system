/*--------------------------------------------------------\
| File: Core.h                                            |
|x-------------------------------------------------------x|
| Details: Rigid body systems' central point - creates the|
| simulation, scene, input handler and user interface.    |
|x-------------------------------------------------------x|
| Date:                                         June 2015 |
| Author:                              Andrius Jefremovas |
| Description: Construct systems and calls their updates. |
\--------------------------------------------------------*/
#ifndef CORE_H
#define CORE_H

//#include "UserInterface.h" // TODO: figure out why it causes stuttering
#include "Simulation.h"
#include "Skybox.h"

namespace rigid_body_sys
{
    class Core
    {
    public:
        Core(float timeStep, unsigned int tickRate);
        ~Core();
        bool Initialise();
        bool Update(const float deltaTime);
        bool Render(const float totalTime);
        void Input(const float deltaTime);
    private:
        float m_accumTime; // Accumulated time
        const float TIME_STEP; // Fixed time slices the physics simulates
        const float TICK_RATE; // Physics updates per second
        //UserInterface* m_userInterface;
        InputHandler* m_inputHandler;
        Simulation* m_simulation;
        Skybox* m_skybox;
    };
}; /* !rigid_body_sys */

#endif /* !CORE_H */