#version 430

uniform sampler2D tex;
uniform vec4 DBG_COL;

layout (location = 0) in vec2 tex_coord;
layout (location = 0) out vec4 colour;

void main()
{
    colour = texture(tex, tex_coord) * DBG_COL;
} 