#version 430

uniform samplerCube cubemap; // Cubemap texture
uniform float time = 0.0f;	// Incrementing time

layout (location = 0) in vec3 tex_coord; // Incoming 3D texture coordinate
layout (location = 0) out vec4 colour; // Outgoing colour

vec3 PulseLineCentered(vec3 uv)
{
    uv = abs(uv*0.125f);
    const vec3 col = vec3(-0.16f);
    const float speed = 0.15f;
    const float offset = 0.1f;
    const float thickness = 1000.0f;
    
    float lineX = (uv.x + offset) * abs(sin(time * speed));
    float lineZ = (uv.z + offset) * abs(sin(time * speed));
    float dy = 1.0f / (thickness * abs((uv.y - lineX) + (uv.y - lineZ)));

    return col*dy;
}
    
void main()
{
    // Texture Colour
    colour = texture(cubemap, tex_coord);
    colour.xyz += PulseLineCentered(tex_coord);
}