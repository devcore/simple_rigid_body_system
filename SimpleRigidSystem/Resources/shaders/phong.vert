#version 430

uniform mat4 M;   // Model matrix
uniform mat4 MVP; // Transformation matrix
uniform mat3 N;   // Normal matrix

// Incoming data
layout (location = 0) in vec3 position;
layout (location = 2) in vec3 normal;
layout (location = 10) in vec2 tex_coord_in;

// Outgoing data
layout (location = 0) out vec3 vertex_position;
layout (location = 1) out vec3 transformed_normal;
layout (location = 2) out vec2 tex_coord_out;

void main()
{
    // Compute world position
	gl_Position = MVP * vec4(position, 1);
    // Also transform vertices for view calculation
	vertex_position = (M * vec4(position, 1)).xyz;
	transformed_normal = N * normal;
	tex_coord_out = tex_coord_in;
}