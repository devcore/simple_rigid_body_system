## **REQUIREMENTS** ##
Microsoft Visual Studio 2012 or newer.

## **SUMMARY** ##
An implementation of a simple [Rigid Body](https://en.wikipedia.org/wiki/Rigid_body) System. Features primitive collision detection of axis aligned bounding boxes(AABB), oriented bounding boxes(OBB), spheres and capsules next, while the collision resolution is handled with impulses.

## **NOTICE** ##
Copyright 2015 Andrius Jefremovas

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.